#!/bin/sh

readonly LEDPHY="$1"
readonly ARGUMENT="$2"
readonly PARAMETER="$3"

if [ "$ARGUMENT" = "setup" -o "$ARGUMENT" = "exit_testmode" ]; then
        # Default
        echo write45 2 "$LEDPHY" 0xa83b 0xa482 > /proc/driver/phy/cmd
        # Green
        echo write45 2 "$LEDPHY" 0xa838 0x80 > /proc/driver/phy/cmd
        echo write45 2 "$LEDPHY" 0xa8f3 0x0 > /proc/driver/phy/cmd
        echo write45 2 "$LEDPHY" 0xa83a 0x0 > /proc/driver/phy/cmd
        # Yellow
        echo write45 2 "$LEDPHY" 0xa832 0x18 > /proc/driver/phy/cmd
        echo write45 2 "$LEDPHY" 0xa8f1 0xc > /proc/driver/phy/cmd
        echo write45 2 "$LEDPHY" 0xa834 0x0 > /proc/driver/phy/cmd

        # Led blink stretch
        echo write45 2 "$LEDPHY" 0xa8ec 0x02ff > /proc/driver/phy/cmd
        rm -rf "/tmp/external_led_testmode_${LEDPHY}"
elif [ "$ARGUMENT" = "enter_testmode" ]; then
        echo write45 2 "$LEDPHY" 0xa832 0x20 > /proc/driver/phy/cmd
        echo write45 2 "$LEDPHY" 0xa838 0x20 > /proc/driver/phy/cmd
        echo write45 2 "$LEDPHY" 0xa83b 0x0 > /proc/driver/phy/cmd
        touch "/tmp/external_led_testmode_${LEDPHY}"
elif [ "$ARGUMENT" = "brightness" ]; then
        if [ "$PARAMETER" = "0" ]; then
                echo write45 2 "$LEDPHY" 0xa832 0x20 > /proc/driver/phy/cmd
                echo write45 2 "$LEDPHY" 0xa838 0x20 > /proc/driver/phy/cmd
                echo write45 2 "$LEDPHY" 0xa83b 0x0 > /proc/driver/phy/cmd
        else
                if [ ! -f "/tmp/external_led_testmode_${LEDPHY}" ]; then
                        # Default
                        echo write45 2 "$LEDPHY" 0xa83b 0xa482 > /proc/driver/phy/cmd
                        # Green
                        echo write45 2 "$LEDPHY" 0xa838 0x80 > /proc/driver/phy/cmd
                        echo write45 2 "$LEDPHY" 0xa8f3 0x0 > /proc/driver/phy/cmd
                        echo write45 2 "$LEDPHY" 0xa83a 0x0 > /proc/driver/phy/cmd
                        # Yellow
                        echo write45 2 "$LEDPHY" 0xa832 0x18 > /proc/driver/phy/cmd
                        echo write45 2 "$LEDPHY" 0xa8f1 0xc > /proc/driver/phy/cmd
                        echo write45 2 "$LEDPHY" 0xa834 0x0 > /proc/driver/phy/cmd

                        # Led blink stretch
                        echo write45 2 "$LEDPHY" 0xa8ec 0x02ff > /proc/driver/phy/cmd
                else
                        echo write45 2 "$LEDPHY" 0xa832 0x20 > /proc/driver/phy/cmd
                        echo write45 2 "$LEDPHY" 0xa838 0x20 > /proc/driver/phy/cmd
                        # 10GE LEDs turn on
                        echo write45 2 "$LEDPHY" 0xa83b 0x2080 > /proc/driver/phy/cmd
                fi
        fi
fi
