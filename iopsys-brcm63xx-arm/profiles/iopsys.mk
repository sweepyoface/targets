# Meta package support
ASTERISK_PACKAGES:=asterisk asterisk-app-playtones asterisk-app-read \
asterisk-app-system asterisk-app-transfer asterisk-cdr \
asterisk-cdr-csv asterisk-chan-brcm asterisk-codec-a-mu \
asterisk-codec-alaw asterisk-codec-g722 asterisk-codec-g726 \
asterisk-codec-gsm asterisk-format-g726 asterisk-format-g729 \
asterisk-format-sln asterisk-func-channel \
asterisk-func-db asterisk-func-shell asterisk-pbx-spool \
asterisk-res-musiconhold asterisk-sounds \
asterisk-format-gsm asterisk-res-rtp-asterisk asterisk-pjsip \
asterisk-util-astcanary asterisk-res-srtp \
asterisk-res-http-websocket asterisk-res-pjproject asterisk-res-sorcery \
asterisk-app-confbridge asterisk-app-voicemail

VOICE_SUPPORT:=endptmngr $(ASTERISK_PACKAGES)
DECT_SUPPORT:=dectmngr

#################### brcm_ref138
define Profile/brcm_ref138_p502
  NAME:=brcm_ref138_p502
  PACKAGES:=$(VOICE_SUPPORT)
endef

define Profile/brcm_ref138_p502/Description
	brcm_ref138 profile
endef

$(eval $(call Profile,brcm_ref138_p502))

#################### DG400PRIME
define Profile/dg400prime
  NAME:=dg400prime
  PACKAGES:=$(VOICE_SUPPORT)
endef

define Profile/dg400prime/Description
	dg400prime profile
endef

$(eval $(call Profile,dg400prime))

#################### TIGER
define Profile/tiger
  NAME:=tiger
  PACKAGES:=i2c-tools
endef

define Profile/tiger/Description
	tiger profile
endef

$(eval $(call Profile,tiger))

#################### PANTHER
define Profile/panther
  NAME:=panther
  PACKAGES:=i2c-tools $(VOICE_SUPPORT)
endef

define Profile/panther/Description
	panther profile
endef

$(eval $(call Profile,panther))

#################### PANDA
define Profile/panda
  NAME:=panda
  PACKAGES:=i2c-tools $(VOICE_SUPPORT)
endef

define Profile/panda/Description
	panda profile
endef

$(eval $(call Profile,panda))

#################### KOALA
define Profile/koala
  NAME:=koala
  PACKAGES:=i2c-tools $(VOICE_SUPPORT)
endef

define Profile/koala/Description
	koala profile
endef

$(eval $(call Profile,koala))

#################### EAGLE
define Profile/eagle
  NAME:=eagle
  PACKAGES:=i2c-tools $(VOICE_SUPPORT)
endef

define Profile/eagle/Description
	eagle profile
endef

$(eval $(call Profile,eagle))

#################### Xavi xug534
define Profile/xug534
  NAME:=xug534
endef

define Profile/xug534/Description
	 Xavi xug534 profile
endef

$(eval $(call Profile,xug534))

#################### smarthub3
define Profile/smarthub3
  NAME:=smarthub3
  PACKAGES:=$(VOICE_SUPPORT) $(DECT_SUPPORT)
endef

define Profile/smarthub3/Description
	smarthub3 profile
endef

$(eval $(call Profile,smarthub3))

#################### disc
define Profile/disc
  NAME:=disc
endef

define Profile/disc/Description
	disc profile
endef

$(eval $(call Profile,disc))
