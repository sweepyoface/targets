# arg1: port name, ex: eth0
get_port_number() {
	[ -z "$1" ] && return
	local ports="0 1 2 3 4 5 6 7"
	local units="0 1"
	local port="$1"
	local ifname

	for unit in $units; do
		for prt in $ports; do
			ifname="$(ethswctl getifname $unit $prt 2>/dev/null | awk '{print$NF}')"
			if [ "$ifname" == "$port" ]; then
				echo "$unit $prt"
				return
			fi
		done
	done
}

# arg1: port ifname, ex: eth0
# arg2: port enabled, ex: 1
power_updown() {
	local ifname="$1"
	local enabled=$2

	local updown="up"
	[ $enabled -eq 0 ] && updown="down"
	ethctl $ifname phy-power $updown >/dev/null
}

# arg1: port ifname, ex: eth0
# arg2: port enabled, ex: 1
# arg3: port speed, ex: 1000
# arg4: port duplex, ex: full
# arg5: port autoneg, ex: on
# arg6: port eee, ex: 0
# arg7: port pause, ex: 0
set_port_settings() {
	local ifname="$1"
	local enabled=$2
	local speed="$3"
	local duplex=$4
	local autoneg=$5
	local eee=$6
	local pause=$7
	local crossbarports

	[ -d /sys/class/net/$ifname ] || return

	local unitport="$(get_port_number $ifname)"
	local unit=$(echo $unitport | cut -d ' ' -f 1)
	local port=$(echo $unitport | cut -d ' ' -f 2)

	[ $autoneg -eq 1 ] && autoneg="on" || autoneg="off"
	[ "$duplex" == "half" ] && duplex=0 || duplex=1
	[ "$duplex" == 0 ] && dplx="HD" || dplx="FD"
	crossbar="$(echo `ethctl $ifname phy-crossbar` | awk -F' ' '{ print $2 $3 }')"

	if [ "$crossbar" == "oncrossbar" ]; then
		crossbarports="$(echo `ethctl $ifname phy-crossbar` | awk -F':' '{ print $NF }')"

		if [ "$autoneg" == "on" ]; then
			for subport in $crossbarports; do
				ethctl $ifname media-type auto port $subport &>/dev/null
			done
		else
			ethctl $ifname media-type $speed$dplx port $subport &>/dev/null
		fi
	else
		if [ "$autoneg" == "on" ]; then
			ethctl $ifname media-type auto &>/dev/null
		else
			ethctl $ifname media-type $speed$dplx &>/dev/null
		fi
	fi

	[ $eee -eq 1 ] && eee="on" || eee="off"
	ethtool --set-eee $ifname eee $eee 2>/dev/null

	case $pause in
		off|0)
			pause=0x0
			auto=off
			rx=off
			tx=off
		;;
		on|1)
			pause=0x2
			auto=off
			rx=on
			tx=on
		;;
		auto)
			pause=0x1
			auto=on
			rx=on
			tx=on
		;;
		tx)
			pause=0x3
			auto=off
			rx=off
			tx=on
		;;
		rx)
			pause=0x4
			auto=off
			rx=on
			tx=off
		;;
	esac
	if [ "$auto" == "on" ]; then
		# Use ethswctl utility to set pause autoneg
		# as ethtool is not setting it properly
		ethswctl -c pause -n $unit -p $port -v $pause 2>&1 >/dev/null
	else
		ethtool --pause $ifname autoneg $auto rx $rx tx $tx 2>/dev/null
	fi

	power_updown $ifname $enabled
}
