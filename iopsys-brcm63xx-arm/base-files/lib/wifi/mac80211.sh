#!/bin/sh
append DRIVERS "mac80211"

lookup_phy() {
	[ -n "$phy" ] && {
		[ -d /sys/class/ieee80211/$phy ] && return
	}

	local devpath
	config_get devpath "$device" path
	[ -n "$devpath" ] && {
		for phy in $(ls /sys/class/ieee80211 2>/dev/null); do
			case "$(readlink -f /sys/class/ieee80211/$phy/device)" in
				*$devpath) return;;
			esac
		done
	}

	local macaddr="$(config_get "$device" macaddr | tr 'A-Z' 'a-z')"
	[ -n "$macaddr" ] && {
		for _phy in /sys/class/ieee80211/*; do
			[ -e "$_phy" ] || continue

			[ "$macaddr" = "$(cat ${_phy}/macaddress)" ] || continue
			phy="${_phy##*/}"
			return
		done
	}
	phy=
	return
}

find_mac80211_phy() {
	local device="$1"

	config_get phy "$device" phy
	lookup_phy
	[ -n "$phy" -a -d "/sys/class/ieee80211/$phy" ] || {
		echo "PHY for wifi device $1 not found"
		return 1
	}
	config_set "$device" phy "$phy"

	config_get macaddr "$device" macaddr
	[ -z "$macaddr" ] && {
		config_set "$device" macaddr "$(cat /sys/class/ieee80211/${phy}/macaddress)"
	}

	return 0
}

check_mac80211_device() {
	config_get phy "$1" phy
	[ -z "$phy" ] && {
		find_mac80211_phy "$1" >/dev/null || return 0
		config_get phy "$1" phy
	}
	[ "$phy" = "$dev" ] && found=1
}

detect_mac80211() {
	local default_ch_pref="11 36 100"
	local default_band_pref="2 5"
	local default_bw_pref="20"
	local apsta="0"

	devidx=0
	config_load wireless

	[ -s "/etc/device_info" ] || return 0
	source "/etc/device_info"	

	while :; do
		config_get type "radio$devidx" type
		[ -n "$type" ] || break
		devidx=$(($devidx + 1))
	done

	# Get factory WPA key.
	local WPAKEY=$(db -q get hw.board.wpa_key)
	WPAKEY=${WPAKEY:-1234567890}
	[ "$WPAKEY" == "00000000" ] && WPAKEY="1234567890"

	BMAC=$(db -q get hw.board.basemac)
	BMAC=${BMAC//:/}
	BMAC=${BMAC// /}

	BSSID=$(printf "%12.12X" $((0x$BMAC)))
	UUID=$(uuidgen -s -r | cut -c 1-24)
	UUID=$UUID$BMAC

	for _dev in /sys/class/ieee80211/*; do
		[ -e "$_dev" ] || continue

		dev="${_dev##*/}"

		found=0
		config_foreach check_mac80211_device wifi-device
		[ "$found" -gt 0 ] && continue

		mode_band=""
		channel=""
		radioidx=""
		htmode="HT20"

		for _netdev in /sys/class/net/*; do
			if [ -h ${_netdev}/phy80211 ] && [ "$(cat ${_netdev}/phy80211/name)" == "${dev}" ]; then
				radioidx="${_netdev##*/}"
				break
			fi
		done

		# check what channels the phy actually supports
		for bw in ${default_bw_pref}; do
			for band in ${default_band_pref}; do
				for ch in ${default_ch_pref}; do
					wl -i ${radioidx} chanspecs -b ${band} -w ${bw} | grep -q "^${ch}[^0-9]" && {
						[ "${band}" == "2" ] && mode_band="g" || mode_band="a"
						channel="${ch}"
						break 3   # 3 nested levels
					};
				done;
			done;
		done;

		res=$(wl -i ${radioidx} he 2>&1 > /dev/null)
		rc=$?
		if [ $rc -eq 0 ]
		then
			[ "${mode_band}" = "a" ] && htmode="HE80" || htmode="HE20"
		else
			[ "${mode_band}" = "a" ] && htmode="VHT80" || htmode="HT20"
		fi

		# This is all fine and dandy but totally unnecessary since we already have the phy

#		if [ -x /usr/bin/readlink -a -h /sys/class/ieee80211/${dev}/device ]; then
#			path="$(readlink -f /sys/class/ieee80211/${dev}/device)"
#		else
#			path=""
#		fi
#		if [ -n "$path" ]; then
#			path="${path##/sys/devices/}"
#			case "$path" in
#				platform*/pci*) path="${path##platform/}";;
#			esac
#			dev_id="set wireless.${radioidx}.path='$path'"
#		else
#			dev_id="set wireless.${radioidx}.macaddr=$(cat ${_netdev}/address)"
#		fi

		dev_id="set wireless.${radioidx}.phy=${dev}"
		rmac=$(iw dev ${radioidx} info | grep addr | cut -d ' ' -f 2)

		uci -q batch <<-EOF
			set wireless.${radioidx}=wifi-device
			set wireless.${radioidx}.type=mac80211
			set wireless.${radioidx}.channel=${channel}
			set wireless.${radioidx}.hwmode=11${mode_band}
			set wireless.${radioidx}.country=DE
			set wireless.${radioidx}.htmode=${htmode}
			set wireless.${radioidx}.apsta=${apsta}
			${dev_id}
			${aps_sta}

			set wireless.default_${radioidx}=wifi-iface
			set wireless.default_${radioidx}.device=${radioidx}
			set wireless.default_${radioidx}.network=lan
			${aps_ap}
			set wireless.default_${radioidx}.mode=ap
			set wireless.default_${radioidx}.ifname='${radioidx}'
			set wireless.default_${radioidx}.ssid="${DEVICE_MANUFACTURER}-${BSSID}"
			set wireless.default_${radioidx}.uuid="${UUID}"
			set wireless.default_${radioidx}.encryption=psk2
			set wireless.default_${radioidx}.key=${WPAKEY}
			set wireless.default_${radioidx}.wps=1
			set wireless.default_${radioidx}.wps_pushbutton=1
			set wireless.default_${radioidx}.ieee80211k=1
			set wireless.default_${radioidx}.ieee80211v=1
			set wireless.default_${radioidx}.bss_transition=1
EOF
		uci -q commit wireless

		devidx=$(($devidx + 1))
	done
}
