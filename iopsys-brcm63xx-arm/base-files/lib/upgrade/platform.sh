platform_check_image() {
	iopsys_check_image $1
}

platform_do_upgrade() {
    local image="$1"
    local status=0
    local skip_bootloader=""

    case "$(get_root_device_type)" in                                                                               
	    nand) local loader_part=/dev/mtd1 ;;
	    emmc) local loader_part=/dev/mmcblk0boot0 ;;                              
	    *) return 1 ;;                                              
    esac                                                                          

    if iopsys_is_bootloader_update_needed "$image" "$loader_part"; then           
        UPGRADE_OPT_FORCE_LOADER_UPGRADE=1
        log "sysupgrade" "Bootloader with newer version is available in FIT-image. Enabling bootloader upgrade."
    fi

    [ -z "$UPGRADE_OPT_FORCE_LOADER_UPGRADE" ] && UPGRADE_OPT_FORCE_LOADER_UPGRADE=1
    if [ "$UPGRADE_OPT_FORCE_LOADER_UPGRADE" = "0" ]; then
        skip_bootloader="-s"
        log "sysupgrade" "Bootloader upgrade is disabled."
    else
        log "sysupgrade" "Bootloader upgrade is enabled."
    fi

    iopsys_process_upgrade_bundle "$image" pre_upgrade || status=1

    set -o pipefail
    [ $status -eq 0 ] && bcm_flasher $skip_bootloader "$image" 2>&1 | log "sysupgrade" || status=$?
    log "sysupgrade" "bcm_flasher returned $status."
    set +o pipefail

    [ $status -eq 0 ] && iopsys_process_upgrade_bundle "$image" post_upgrade || status=1

    iopsys_clean_other_overlay || status=1

    [ $status -eq 0 ] && return 0 || return 1
}

platform_copy_config() {
    iopsys_copy_config
}

# Tell the bootloader to boot from flash "bank" $1 next time.
# $1: bank identifier (1 or 2)
platform_set_bootbank() {
# bcm_bootstate BOOT_SET_PART1_IMAGE_ONCE selects rootfs1 (bank id 1) to be booted once on next boot
# bcm_bootstate BOOT_SET_PART2_IMAGE_ONCE selects rootfs2 (bank id 2) to be booted once on next boot
# init-script in the new image will set boot state permanently once it is booted
    local bank_id="$1"
    local status
    set -o pipefail
    case "$bank_id" in
    1|2)
        bcm_bootstate BOOT_SET_PART${bank_id}_IMAGE_ONCE 2>&1 | log sysupgrade
        ;;
    *)
        log sysupgrade "Illegal bank id: $bank_id."
        return 1
        ;;
    esac;
    status=$?
    set +o pipefail
    # We store the value that we SET here, because bcm_bootstate does not return
    # the correct value when the system has been powered off and on.
    # It only works when there has been a reset_reason like normal reboot for example.
    # The bcm_bootstate utility uses /proc/bootstate (bcm_bootstate module).
    # The way that this works is that each bank has a commit-flag, in the metadata-volume.
    # This determines which bank is the one to boot.
    # To facilitate booting another bank once, a flag will be set using
    # the bcm_bootstate-driver in HW.
    # Retrieving this flag does not work if the reset_reason was PWR_ON.
    # Writing to reset_reason works, but in case of power off/on, the driver
    # always returns the hardcoded value of 0xffffffff.
    # This seems to be by design, but right now it is not clear,
    # whether there is some limitation behind it or whether it is a design miss.
    #
    # Note that this workaround here does not catch someone (a developer) setting
    # the bank manually using the bcm_bootstate command outside of this function,
    # if set_bootbank has at least been called once.
    if [ $status -eq 0 ]; then
        echo -n "$1" > /var/run/bcm_bootbank || return 1
        return 0
    else
        return 1
    fi
}

#--------------------------------------------------------------
# Return what "bank" the bootloader will select next time.
platform_get_bootbank() {
    # See comment in platform_set_bootbank for the reason for
    # this file existing
    stored_bcm_bootbank="$(cat /var/run/bcm_bootbank 2>/dev/null)"
    if [ -n "$stored_bcm_bootbank" ]; then
        echo -n "$stored_bcm_bootbank"
        return 0
    fi

    # Call bcm_bootstate only once.
    bootstate_output=$(bcm_bootstate | grep "Reboot Partition")
    if echo "$bootstate_output" | grep -q First; then
        echo -n "1"
        return 0
    fi

    if echo "$bootstate_output" | grep -q Second; then
        echo -n "2"
        return 0
    fi

    # return error
    return 1
}

# Optional API. Retun the associated iopsys version for a particular bank
# useful when the inactive rootfs could be encrypted
# $1: bank identifier (1 or 2)
platform_get_iopsys_version() {
    local bootfs_device
    local bank_id=$1
    local dev_prefix="ubi0_"

    [ -e /dev/mmcblk0 ] && dev_prefix="mmcblk0p"

    case $bank_id in
    1)
        bootfs_device="/dev/${dev_prefix}3" ;;
    2)
        bootfs_device="/dev/${dev_prefix}5" ;;
    esac

    # Get the iopsys version from the FIT image located on the bootfs.
    fdtextract -a iopsys_version $bootfs_device

    return $?
}
