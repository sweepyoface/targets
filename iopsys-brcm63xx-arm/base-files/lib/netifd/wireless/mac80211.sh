#!/bin/sh
. /lib/netifd/netifd-wireless.sh
. /lib/netifd/hostapd.sh

init_wireless_driver "$@"

MP_CONFIG_INT="mesh_retry_timeout mesh_confirm_timeout mesh_holding_timeout mesh_max_peer_links
	       mesh_max_retries mesh_ttl mesh_element_ttl mesh_hwmp_max_preq_retries
	       mesh_path_refresh_time mesh_min_discovery_timeout mesh_hwmp_active_path_timeout
	       mesh_hwmp_preq_min_interval mesh_hwmp_net_diameter_traversal_time mesh_hwmp_rootmode
	       mesh_hwmp_rann_interval mesh_gate_announcements mesh_sync_offset_max_neighor
	       mesh_rssi_threshold mesh_hwmp_active_path_to_root_timeout mesh_hwmp_root_interval
	       mesh_hwmp_confirmation_interval mesh_awake_window mesh_plink_timeout"
MP_CONFIG_BOOL="mesh_auto_open_plinks mesh_fwding"
MP_CONFIG_STRING="mesh_power_mode"

iw() {
	command iw $@ || logger -t mac80211 "Failed command: iw $@"
}

drv_mac80211_init_device_config() {
	hostapd_common_add_device_config

	config_add_string path phy 'macaddr:macaddr'
	config_add_string hwmode
	config_add_string tx_burst
	config_add_int beacon_int chanbw frag rts
	config_add_int rxantenna txantenna antenna_gain txpower distance
	config_add_boolean noscan ht_coex
	config_add_array ht_capab
	config_add_array channels
	config_add_boolean \
		rxldpc \
		short_gi_80 \
		short_gi_160 \
		tx_stbc_2by1 \
		su_beamformer \
		su_beamformee \
		mu_beamformer \
		mu_beamformee \
		vht_txop_ps \
		htc_vht \
		rx_antenna_pattern \
		tx_antenna_pattern
	config_add_int vht_max_a_mpdu_len_exp vht_max_mpdu vht_link_adapt vht160 rx_stbc tx_stbc
	config_add_boolean \
		ldpc \
		greenfield \
		short_gi_20 \
		short_gi_40 \
		max_amsdu \
		dsss_cck_40
	config_add_int apsta
	config_add_int country_rev
	config_add_boolean acs_exclude_dfs
	config_add_int acs_refresh_period
	config_add_boolean \
		he_su_beamformer \
		he_su_beamformee \
		he_mu_beamformer \
		he_dl_ofdma \
		he_ul_ofdma \
		he_dl_mumimo
}

drv_mac80211_init_iface_config() {
	hostapd_common_add_bss_config

	config_add_string 'macaddr:macaddr' ifname

	config_add_boolean wds powersave
	config_add_int maxassoc
	config_add_int max_listen_int
	config_add_int dtim_period
	config_add_int start_disabled

	# mesh
	config_add_string mesh_id
	config_add_int $MP_CONFIG_INT
	config_add_boolean $MP_CONFIG_BOOL
	config_add_string $MP_CONFIG_STRING
}

bcm_wl_set() {
	local ifname=$1
	local cmd=$2
	local param=$3

	local com="wl -i $ifname $cmd $param"
	logger -t mac80211 "execute: $com"
	out=$($com 2>&1)
	res=$?

	[ "$res" -ne 0 ] && {
		logger -t mac80211 "$com failed with res ${res} and out ${out}"
	}
}

bcm_wl_set_clear_cap() {
	local ifname=$1
	local comm=$2
	local value=$3
	local setme=$4

	local current=$(wl -i $ifname $comm | cut -d' ' -f1)
	logger -t mac80211 "$ifname $comm current $current value $value setme $setme"
	if [ "$setme" -eq 1 ]; then
		new=$(( $current | $value ))
	else
		new=$(( $current & ~$value ))
	fi

	out=$(wl -i $ifname $comm $new 2>&1)
	res=$?

	local after_set=$(wl -i $ifname $comm)
	logger -t mac80211 "$ifname $comm set $new vs $after_set res $res out $out"
}

bcm_add_radio_capabilities() {
	ifname=$1
	htmode=$2

	json_select config
	json_get_vars \
		ldpc:1 \
		greenfield:0 \
		short_gi_20:1 \
		short_gi_40:1 \
		tx_stbc:-1 \
		rx_stbc:1 \
		max_amsdu:1 \
		dsss_cck_40:1
	json_get_vars \
		rxldpc:1 \
		short_gi_80:1 \
		short_gi_160:1 \
		tx_stbc_2by1:1 \
		su_beamformer:1 \
		su_beamformee:1 \
		mu_beamformer:1 \
		mu_beamformee:1 \
		vht_txop_ps:1 \
		htc_vht:1 \
		rx_antenna_pattern:1 \
		tx_antenna_pattern:1 \
		vht_max_a_mpdu_len_exp:7 \
		vht_max_mpdu:11454 \
		vht_link_adapt:3 \
		vht160:2
	json_get_vars \
		he_su_beamformer:1 \
		he_su_beamformee:1 \
		he_mu_beamformer:1 \
		he_mu_beamformee:1 \
		he_dl_ofdma:1 \
		he_ul_ofdma:1 \
		he_dl_mumimo:1
	json_select ..

	bcm_wl_set "$ifname" "ldpc_cap" "$ldpc"
	bcm_wl_set "$ifname" "gf_cap" "$greenfield"
	bcm_wl_set "$ifname" "stbc_tx" "$tx_stbc"
	bcm_wl_set "$ifname" "stbc_rx" "$rx_stbc"
	bcm_wl_set "$ifname" "amsdu_rxmax" "$max_amsdu"

	bcm_wl_set_clear_cap "$ifname" sgi_rx 1 $short_gi_20
	bcm_wl_set_clear_cap "$ifname" sgi_rx 2 $short_gi_40
	bcm_wl_set_clear_cap "$ifname" sgi_rx 4 $short_gi_80
	bcm_wl_set_clear_cap "$ifname" sgi_rx 8 $short_gi_160

	# multi user will also set single user
	# TODO check if he settings also force vht
	bcm_wl_set_clear_cap "$ifname" txbf_bfr_cap 2 $mu_beamformer
	bcm_wl_set_clear_cap "$ifname" txbf_bfe_cap 2 $mu_beamformee

	bcm_wl_set_clear_cap "$ifname" txbf_bfr_cap 1 $su_beamformer
	bcm_wl_set_clear_cap "$ifname" txbf_bfe_cap 1 $su_beamformee

	bcm_wl_set_clear_cap "$ifname" txbf_bfr_cap 8 $he_mu_beamformer
	bcm_wl_set_clear_cap "$ifname" txbf_bfe_cap 8 $he_mu_beamformee

	bcm_wl_set_clear_cap "$ifname" txbf_bfr_cap 4 $he_su_beamformer
	bcm_wl_set_clear_cap "$ifname" txbf_bfe_cap 4 $he_su_beamformee

	bcm_wl_set_clear_cap "$ifname" "he features" 4 $he_dl_ofdma
	bcm_wl_set_clear_cap "$ifname" "he features" 8 $he_ul_ofdma
	bcm_wl_set_clear_cap "$ifname" "he features" 16 $he_dl_mumimo
}

mac80211_add_capabilities() {
	local __var="$1"; shift
	local __mask="$1"; shift
	local __out= oifs

	oifs="$IFS"
	IFS=:
	for capab in "$@"; do
		set -- $capab

		[ "$(($4))" -gt 0 ] || continue
		[ "$(($__mask & $2))" -eq "$((${3:-$2}))" ] || continue
		__out="$__out[$1]"
	done
	IFS="$oifs"

	export -n -- "$__var=$__out"
}

mac80211_hostapd_setup_base() {
	local phy="$1"

	json_select config

	[ "$auto_channel" -gt 0 ] && channel=acs_survey
	[ "$auto_channel" -gt 0 ] && json_get_values channel_list channels

	json_get_vars noscan ht_coex
	json_get_values ht_capab_list ht_capab tx_burst

	set_default noscan 0

	[ "$noscan" -gt 0 ] && hostapd_noscan=1
	[ "$tx_burst" = 0 ] && tx_burst=

	false && {
	ieee80211n=1
	ht_capab=
	case "$htmode" in
		VHT20|HT20) ;;
		HT40*|VHT40|VHT80|VHT160)
			case "$hwmode" in
				a)
					case "$(( ($channel / 4) % 2 ))" in
						1) ht_capab="[HT40+]";;
						0) ht_capab="[HT40-]";;
					esac
				;;
				*)
					case "$htmode" in
						HT40+) ht_capab="[HT40+]";;
						HT40-) ht_capab="[HT40-]";;
						*)
							if [ "$channel" -lt 7 ]; then
								ht_capab="[HT40+]"
							else
								ht_capab="[HT40-]"
							fi
						;;
					esac
				;;
			esac
			[ "$auto_channel" -gt 0 ] && ht_capab="[HT40+]"
		;;
		*) ieee80211n= ;;
	esac

	[ -n "$ieee80211n" ] && {
		append base_cfg "ieee80211n=1" "$N"

		set_default ht_coex 0
		append base_cfg "ht_coex=$ht_coex" "$N"

		json_get_vars \
			ldpc:1 \
			greenfield:0 \
			short_gi_20:1 \
			short_gi_40:1 \
			tx_stbc:1 \
			rx_stbc:3 \
			max_amsdu:1 \
			dsss_cck_40:1

		ht_cap_mask=0
		for cap in $(iw phy "$phy" info | grep 'Capabilities:' | cut -d: -f2); do
			ht_cap_mask="$(($ht_cap_mask | $cap))"
		done

		cap_rx_stbc=$((($ht_cap_mask >> 8) & 3))
		[ "$rx_stbc" -lt "$cap_rx_stbc" ] && cap_rx_stbc="$rx_stbc"
		ht_cap_mask="$(( ($ht_cap_mask & ~(0x300)) | ($cap_rx_stbc << 8) ))"

		mac80211_add_capabilities ht_capab_flags $ht_cap_mask \
			LDPC:0x1::$ldpc \
			GF:0x10::$greenfield \
			SHORT-GI-20:0x20::$short_gi_20 \
			SHORT-GI-40:0x40::$short_gi_40 \
			TX-STBC:0x80::$tx_stbc \
			RX-STBC1:0x300:0x100:1 \
			RX-STBC12:0x300:0x200:1 \
			RX-STBC123:0x300:0x300:1 \
			MAX-AMSDU-7935:0x800::$max_amsdu \
			DSSS_CCK-40:0x1000::$dsss_cck_40

		ht_capab="$ht_capab$ht_capab_flags"
		[ -n "$ht_capab" ] && append base_cfg "ht_capab=$ht_capab" "$N"
	}

	# 802.11ac
	enable_ac=0
	idx="$channel"
	case "$htmode" in
		VHT20) enable_ac=1;;
		VHT40)
			case "$(( ($channel / 4) % 2 ))" in
				1) idx=$(($channel + 2));;
				0) idx=$(($channel - 2));;
			esac
			enable_ac=1
			append base_cfg "vht_oper_chwidth=0" "$N"
			append base_cfg "vht_oper_centr_freq_seg0_idx=$idx" "$N"
		;;
		VHT80)
			case "$(( ($channel / 4) % 4 ))" in
				1) idx=$(($channel + 6));;
				2) idx=$(($channel + 2));;
				3) idx=$(($channel - 2));;
				0) idx=$(($channel - 6));;
			esac
			enable_ac=1
			append base_cfg "vht_oper_chwidth=1" "$N"
			append base_cfg "vht_oper_centr_freq_seg0_idx=$idx" "$N"
		;;
		VHT160)
			case "$channel" in
				36|40|44|48|52|56|60|64) idx=50;;
				100|104|108|112|116|120|124|128) idx=114;;
			esac
			enable_ac=1
			append base_cfg "vht_oper_chwidth=2" "$N"
			append base_cfg "vht_oper_centr_freq_seg0_idx=$idx" "$N"
		;;
	esac

	if [ "$enable_ac" != "0" ]; then
		json_get_vars \
			rxldpc:1 \
			short_gi_80:1 \
			short_gi_160:1 \
			tx_stbc_2by1:1 \
			su_beamformer:1 \
			su_beamformee:1 \
			mu_beamformer:1 \
			mu_beamformee:1 \
			vht_txop_ps:1 \
			htc_vht:1 \
			rx_antenna_pattern:1 \
			tx_antenna_pattern:1 \
			vht_max_a_mpdu_len_exp:7 \
			vht_max_mpdu:11454 \
			rx_stbc:4 \
			vht_link_adapt:3 \
			vht160:2

		set_default tx_burst 2.0
		append base_cfg "ieee80211ac=1" "$N"
		vht_cap=0
		for cap in $(iw phy "$phy" info | awk -F "[()]" '/VHT Capabilities/ { print $2 }'); do
			vht_cap="$(($vht_cap | $cap))"
		done

		cap_rx_stbc=$((($vht_cap >> 8) & 7))
		[ "$rx_stbc" -lt "$cap_rx_stbc" ] && cap_rx_stbc="$rx_stbc"
		vht_cap="$(( ($vht_cap & ~(0x700)) | ($cap_rx_stbc << 8) ))"

		mac80211_add_capabilities vht_capab $vht_cap \
			RXLDPC:0x10::$rxldpc \
			SHORT-GI-80:0x20::$short_gi_80 \
			SHORT-GI-160:0x40::$short_gi_160 \
			TX-STBC-2BY1:0x80::$tx_stbc_2by1 \
			SU-BEAMFORMER:0x800::$su_beamformer \
			SU-BEAMFORMEE:0x1000::$su_beamformee \
			MU-BEAMFORMER:0x80000::$mu_beamformer \
			MU-BEAMFORMEE:0x100000::$mu_beamformee \
			VHT-TXOP-PS:0x200000::$vht_txop_ps \
			HTC-VHT:0x400000::$htc_vht \
			RX-ANTENNA-PATTERN:0x10000000::$rx_antenna_pattern \
			TX-ANTENNA-PATTERN:0x20000000::$tx_antenna_pattern \
			RX-STBC-1:0x700:0x100:1 \
			RX-STBC-12:0x700:0x200:1 \
			RX-STBC-123:0x700:0x300:1 \
			RX-STBC-1234:0x700:0x400:1 \

		# supported Channel widths
		vht160_hw=0
		[ "$(($vht_cap & 12))" -eq 4 -a 1 -le "$vht160" ] && \
			vht160_hw=1
		[ "$(($vht_cap & 12))" -eq 8 -a 2 -le "$vht160" ] && \
			vht160_hw=2
		[ "$vht160_hw" = 1 ] && vht_capab="$vht_capab[VHT160]"
		[ "$vht160_hw" = 2 ] && vht_capab="$vht_capab[VHT160-80PLUS80]"

		# maximum MPDU length
		vht_max_mpdu_hw=3895
		[ "$(($vht_cap & 3))" -ge 1 -a 7991 -le "$vht_max_mpdu" ] && \
			vht_max_mpdu_hw=7991
		[ "$(($vht_cap & 3))" -ge 2 -a 11454 -le "$vht_max_mpdu" ] && \
			vht_max_mpdu_hw=11454
		[ "$vht_max_mpdu_hw" != 3895 ] && \
			vht_capab="$vht_capab[MAX-MPDU-$vht_max_mpdu_hw]"

		# maximum A-MPDU length exponent
		vht_max_a_mpdu_len_exp_hw=0
		[ "$(($vht_cap & 58720256))" -ge 8388608 -a 1 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=1
		[ "$(($vht_cap & 58720256))" -ge 16777216 -a 2 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=2
		[ "$(($vht_cap & 58720256))" -ge 25165824 -a 3 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=3
		[ "$(($vht_cap & 58720256))" -ge 33554432 -a 4 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=4
		[ "$(($vht_cap & 58720256))" -ge 41943040 -a 5 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=5
		[ "$(($vht_cap & 58720256))" -ge 50331648 -a 6 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=6
		[ "$(($vht_cap & 58720256))" -ge 58720256 -a 7 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=7
		vht_capab="$vht_capab[MAX-A-MPDU-LEN-EXP$vht_max_a_mpdu_len_exp_hw]"

		# whether or not the STA supports link adaptation using VHT variant
		vht_link_adapt_hw=0
		[ "$(($vht_cap & 201326592))" -ge 134217728 -a 2 -le "$vht_link_adapt" ] && \
			vht_link_adapt_hw=2
		[ "$(($vht_cap & 201326592))" -ge 201326592 -a 3 -le "$vht_link_adapt" ] && \
			vht_link_adapt_hw=3
		[ "$vht_link_adapt_hw" != 0 ] && \
			vht_capab="$vht_capab[VHT-LINK-ADAPT-$vht_link_adapt_hw]"

		[ -n "$vht_capab" ] && append base_cfg "vht_capab=$vht_capab" "$N"
	fi
	}

	hostapd_prepare_device_config "$hostapd_conf_file" nl80211
	cat >> "$hostapd_conf_file" <<EOF
${channel:+channel=$channel}
${channel_list:+chanlist=$channel_list}
${tx_burst:+tx_queue_data2_burst=$tx_burst}
$base_cfg

EOF
	json_select ..
}

bcm_setup_sta() {
	local ifname=$1

	json_get_vars multi_ap

	echo "bcm_setup_sta $ifname multi_ap $multi_ap"

	radio=$(echo "$ifname" | cut -d. -f1)

	[ "$radio" != "$ifname" ] && {
		echo "only radio netdev allowed for STA"
	}

	isup=$(wl -i "$radio" isup)
	[ "$isup" != "0" ] && {
		echo "bcm_setup_sta $ifname wl $radio already up - down it"
		wl -i "$radio" down
	}

	case "$multi_ap" in
		1)
			wl -i "$ifname" map 4
		;;
		*)
			wl -i "$ifname" map 0
		;;
	esac

	[ "$isup" != "0" ] && {
		echo "bcm_setup_sta $ifname wl $radio restore up"
		wl -i "$radio" up
	}

	wl -i "$radio" up
}

bcm_setup_bss() {
	local ifname=$1

	json_get_vars multi_ap ieee80211k ieee80211v multicast_to_unicast mbo

	[ -z "$multicast_to_unicast" ] && multicast_to_unicast=0

	echo "bcm_setup_bss $ifname multi_ap $multi_ap 11v $ieee80211v 11k $ieee80211k mcast2ucast $multicast_to_unicast mbo $mbo"

	radio=$(echo "$ifname" | cut -d. -f1)
	isup=$(wl -i "$ifname" isup)
	[ "$isup" != "0" ] && {
		echo "bcm_setup_bss $ifname wl $radio already up - down it"
		wl -i "$radio" down
	}

	case "$multi_ap" in
		1)
			# backhaul AP
			wl -i "$ifname" map 2
		;;
		2)
			# fronthaul AP
			wl -i "$ifname" map 1
		;;
		3)
			# backhaul + fronthaul AP
			wl -i "$ifname" map 3
		;;
		*)
			wl -i "$ifname" map 0
		;;
	esac

	case "$ieee80211k" in
		1)
			# Link_Measurement Neighbor_Report Beacon_Passive Beacon_Active Beacon_Table
			wl -i "$ifname" rrm 0x73
		;;
		*)
			wl -i "$ifname" rrm 0
		;;
	esac

	case "$ieee80211v" in
		1)
			# BSS-Transition Notification
			wl -i "$ifname" wnm 0x101
		;;
		*)
			wl -i "$ifname" wnm 0
		;;
	esac

	case "$mbo" in
		1)
			wl -i "$ifname" mbo ap_enable 1
		;;
		*)
			wl -i "$ifname" mbo ap_enable 0
		;;
	esac

	[ -n "$multicast_to_unicast" ] && {
		dhdctl -i "$ifname" wmf_bss_enable "$multicast_to_unicast"
		wl -i "$ifname" wmf_bss_enable "$multicast_to_unicast"
	}

	[ "$isup" != "0" ] && {
		echo "bcm_setup_bss $ifname wl $radio restore up"
		wl -i "$radio" up
	}
}

mac80211_hostapd_setup_bss() {
	local phy="$1"
	local ifname="$2"
	local macaddr="$3"
	local type="$4"

	hostapd_cfg=
	append hostapd_cfg "$type=$ifname" "$N"

	hostapd_set_bss_options hostapd_cfg "$vif" || return 1
	json_get_vars wds dtim_period max_listen_int start_disabled

	set_default wds 0
	set_default start_disabled 0

	[ "$wds" -gt 0 ] && append hostapd_cfg "wds_sta=1" "$N"
	[ "$staidx" -gt 0 -o "$start_disabled" -eq 1 ] && append hostapd_cfg "start_disabled=1" "$N"

	cat >> /var/run/hostapd-$phy.conf <<EOF
$hostapd_cfg
bssid=$macaddr
${dtim_period:+dtim_period=$dtim_period}
${max_listen_int:+max_listen_interval=$max_listen_int}
EOF
}

mac80211_get_addr() {
	local phy="$1"
	local idx="$(($2 + 1))"

	head -n $(($macidx + 1)) /sys/class/ieee80211/${phy}/addresses | tail -n1
}

mac80211_generate_mac() {
	local ifname="$1"
	local macidx="$2"

	radio=$(echo "$ifname" | cut -d. -f1)

	local ref="$(wl -i $radio perm_etheraddr | awk '{print tolower($2)}')"
	local bssmax="$(wl -i $radio bssmax)"

	max=$((($bssmax - 1) & 0xff))
	maxn=$(((~$max) & 0xff))


	[ "$ifname" == "$radio" ] && {
		echo "$ref"
		return
	}

	# base on ifname to be sure we set correct/same bssid
	# even someone disable iface in UCI
	macidx=$(echo "$ifname" | cut -d. -f2)

	oIFS="$IFS"; IFS=":"; set -- $ref; IFS="$oIFS"

	b1=$(( ((0x$6 & 0x3f) << 2) | 0x02 ))
	step1=$(( 0x$6 & $maxn ))
	step2=$(( (0x$6 & $max) + $macidx ))
	step2=$(( $step2 % $bssmax ))
	b6=$(( $step1 | $step2 ))

	printf "%02x:%s:%s:%s:%s:%02x" $b1 $2 $3 $4 $5 $b6
}

find_phy() {
	[ -n "$phy" -a -d /sys/class/ieee80211/$phy ] && return 0
	[ -n "$path" ] && {
		for phy in $(ls /sys/class/ieee80211 2>/dev/null); do
			case "$(readlink -f /sys/class/ieee80211/$phy/device)" in
				*$path) return 0;;
			esac
		done
	}
	[ -n "$macaddr" ] && {
		for phy in $(ls /sys/class/ieee80211 2>/dev/null); do
			grep -i -q "$macaddr" "/sys/class/ieee80211/${phy}/macaddress" && return 0
		done
	}
	return 1
}

mac80211_check_ap() {
	has_ap=1
}

bcm_get_chanspec() {
	ifname=$1

	chanspec=$(wl -i "$ifname" chanspec)
	echo "$chanspec"
}

bcm_get_channel() {
	ifname=$1

	chanspec=$(wl -i "$ifname" chanspec | grep "\/")
	if [ -n "$chanspec" ]; then
		echo "$chanspec" | cut -d\/ -f1
		return
	fi

	chanspec=$(wl -i "$ifname" chanspec)
	echo "$chanspec" | cut -d" " -f1
}

bcm_get_bw() {
	ifname=$1

	chanspec=$(wl -i "$ifname" chanspec | grep "\/")
	if [ -n "$chanspec" ]; then
		echo "$chanspec" | cut -d\/ -f2 | cut -d" " -f1
		return
	fi

	echo "20"
}

bcm_auto_channel() {
	phy=$1
	hwmode=$2
	htmode=$3
	acs_xdfs=$4
	acs_int=$5
	channel_list=$6

	phyidx=$(echo "$phy" | sed 's/\(phy\)\([0-9]*\)/\2/g')
	ifname=$(iw dev | grep phy#${phyidx} -A1 | grep Interface | cut -d' ' -f2)

	echo "bcm_auto_channel $ifname hwmode $hwmode htmode $htmode"
	case "$hwmode" in
		a) band=5g ;;
		g) band=2g ;;
		*) band=auto ;;
	esac

	case "$htmode" in
		*20) bw_cap=0x1 ;;
		*40) bw_cap=0x3 ;;
		*80) bw_cap=0x7 ;;
		*160) bw_cap=0xf ;;
		*) bw_cap=0x1 ;;
	esac

	echo "bcm_auto_channel $ifname setup $band bw_cap $bw_cap"
	wl -i "$ifname" band "$band"
	wl -i "$ifname" bw_cap "$band" "$bw_cap"

	channel=$(bcm_get_channel $ifname)
	echo "bcm_auto_channel channel $channel htmode $htmode"

	acsif=$(echo "$ifname" | cut -d. -f1)
	nvram set "$acsif"_reg_mode=h
        nvram set "$acsif"_max_acs=10
	nvram set "$acsif"_bss_enabled=1
	[ "$acs_xdfs" == "0" ] && acs_dfs=0 || acs_dfs=2
	nvram set "$acsif"_acs_dfs=$acs_dfs
	[ "$acs_int" == "0" ] && nvram set "$acsif"_acs_boot_only=1 || {
		nvram unset "$acsif"_acs_boot_only
		[ $acs_int -ge 600 ] && nvram set "$acsif"_acs_cs_scan_timer=$acs_int
	}

	# setup acs_pref_chans
	[ -n "$channel_list" ] && {
		logger -t mac80211 "$ifname channel_list $channel_list"

		# get full list of possible chanspecs
		chanspecs=$(wl -i $ifname chanspecs)

		# expand list of requested channels
		for item in $channel_list; do
			for ch in $(seq ${item%%-*} ${item##*-}); do
				[ -z "$chanlist" ] && chanlist="$ch" || chanlist="$chanlist $ch"
			done
		done

		# exclude undesired channels
		local chanspec_ch chanspec_match chanlist_excl
		for spec in $chanspecs; do
			chanspec_ch=${spec%%[!0-9]*}
			if [ -z "$chanspec_ch" ]; then
				# this is the chanspec hex
				if [ -n "$chanspec_match" ]; then
					#echo "$spec matches!"
					unset chanspec_match
				else
					# exclude this chanspec
					local chanspec_hex=$(echo $spec | tr -d '()')
					#echo "add $chanspec_hex to exclude list"
					[ -z "$chanlist_excl" ] && chanlist_excl="$chanspec_hex" || chanlist_excl="$chanlist_excl,$chanspec_hex"
				fi
			else
				# this is the chanspec channel
				for ch in $chanlist; do
					if [ $ch -eq $chanspec_ch ]; then
						chanspec_match=true
						#echo "$ch matches!"
						break
					fi
				done
			fi
		done
	}
	nvram set "$ifname"_acs_excl_chans=$chanlist_excl

	# make sure ACS will start correctly after reboot
	nvram unset "$ifname"_chanspec

	# Finally cleanup auto channel - don't confuse hostapd
	auto_channel=0
}

bcm_set_legacy_rates() {
	ifname=$1
	hw_mode=$2
	legacy_rates=$3

	logger -t mac80211 "bcm_set_legacy_rates $ifname $hw_mode legacy $legacy_rates"
	case "$hwmode" in
		g)
			isup=$(wl -i "$ifname" isup)
			[ "$isup" != "0" ] && {
				echo "bcm_set_legacy_rates $ifname already up - down it"
				wl -i "$ifname" down
			}

			if [ $legacy_rates -eq 0 ]; then
				wl -i "$ifname" rateset 6b 9 12 18 24b 36 48 54
			else
				wl -i "$ifname" rateset 1b 2b 5.5b 6 9 11b 12 18 24 36 48 54
			fi

			[ "$isup" != "0" ] && {
				echo "bcm_set_legacy_rates $ifname restore up"
				wl -i "$ifname" up
			}
		;;
		*)
		;;
	esac
}

bcm_set_chanspec() {
	ifname=$1
	channel=$2
	htmode=$3
	hwmode=$4

	echo "bcm_set_chanspec $ifname change channel $channel htmode $htmode hwmode $hwmode"

	case "$hwmode" in
		a) band=5g ;;
		g) band=2g ;;
		*) band=auto ;;
	esac

	case "$htmode" in
		HT20|VHT20|HE20)
			chanspec="${channel}"
			bw_cap=0x1
		;;
		HT40+|VHT40+|HE40+)
			chanspec="${channel}l"
			bw_cap=0x3
		;;
		HT40-|VHT40-|HE40-)
			chanspec="${channel}u"
			bw_cap=0x3
		;;
		HT40|VHT40|HE40)
			bw_cap=0x3
			if [ "$band" = "5g" ]; then
				chanspec=${channel}/40
			else
				if [ $channel -gt 5 ]; then
					chanspec="${channel}u"
				else
					chanspec="${channel}l"
				fi
			fi
		;;
		VHT80|HE80)
			chanspec="${channel}/80"
			bw_cap=0x7
		;;
		VHT160|HE160)
			chanspec="${channel}/160"
			bw_cap=0xf
		;;
		*)
			chanspec="${channel}"
			bw_cap=0x1
		;;
	esac

	# nmode = -1 - AUTO
	# nmode = 0  - OFF
	case "$htmode" in
		HE*)
			he=1
			vhtmode=1
			nmode=-1
		;;
		VHT*)
			he=0
			vhtmode=1
			nmode=-1
		;;
		HT*)
			he=0
			vhtmode=0
			nmode=-1
		;;
		*)
			he=0
			vhtmode=0
			nmode=0
		;;
	esac

	# even HW support this, disable VHT for 2.4GHz
	[ "$band" = "2g" ] && vhtmode=0

	isup=$(wl -i "$ifname" isup)
	[ "$isup" != "0" ] && {
		echo "bcm_set_chanspec $ifname already up - down it"
		wl -i "$ifname" down
	}

	wl -i "$ifname" band "$band"
	wl -i "$ifname" bw_cap "$band" "$bw_cap"
	echo "bcm_set_chanspec $ifname setup $band bw_cap $bw_cap"

	wl -i "$ifname" nmode "$nmode" || echo "$ifname nmode $nmode failed"
	wl -i "$ifname" vhtmode "$vhtmode" || echo "$ifname vhtmode $vhtmode failed"
	wl -i "$ifname" he "$he" || echo "$ifname he $he failed"
	wl -i "$ifname" chanspec "$chanspec"
	echo "bcm_set_chanspec $ifname chanspec $chanspec he/vht/n: $he/$vhtmode/$nmode"

	#nvram set "${ifname}_chanspec=${chanspec}"

	# DFS related - when CAC ongoning, don't kill hostapd (wait)
	wl -i "$ifname" keep_ap_up 1

	[ "$isup" != "0" ] && {
		echo "bcm_set_chanspec $ifname restore up"
		wl -i "$ifname" up
	}

	chanspec=$(bcm_get_chanspec "$ifname")
	logger -t mac80211 "bcm_set_chanspec $ifname - set $chanspec"
}

brcm_service_start() {

	# Auto Channel #
	acs_ifnames="$( uci show wireless | grep '\.channel' | grep auto | cut -d'.' -f2 | tr '\n' ' ' | sort -u)"
	nvram set acs_ifnames="$acs_ifnames"

	killall -9 acs_cli2
	killall -9 acsd2
	acsd2 2>/dev/null &
	sleep 1
	for acsif in $(nvram get acs_ifnames); do
		acs_cli2 -i $acsif set mode 2
	done
	###############
}

mac80211_iw_interface_add() {
	local phy="$1"
	local ifname="$2"
	local type="$3"
	local wdsflag="$4"
	local rc

	radio=$(echo "$ifname" | cut -d. -f1)

	echo "interface_add $phy $ifname $type $wdsflag"
	info=$(command iw "$ifname" info)
	rc="$?"

	[ "$rc" == 0 ] && {
		echo "$ifname already exists set type $type"
		command iw "$ifname" set type $type
		rc="$?"
		echo "$ifname set type status $rc"

		# activate brcm DFS offload
		if [ "$type" = "__ap" ]; then
			ifconfig "$ifname" up
			ifconfig "$ifname" down
		fi

		return $rc
	}

	# interface add works only when main/radio netdev UP
	ifconfig "$radio" up

	command iw phy "$phy" interface add "$ifname" type "$type" $wdsflag
	rc="$?"

	[ "$rc" = 237 ] && {
		command iw "$ifname" set type $type
		rc="$?"
	}

	[ "$rc" = 233 ] && {
		# Device might have just been deleted, give the kernel some time to finish cleaning it up
		sleep 1

		command iw phy "$phy" interface add "$ifname" type "$type" $wdsflag
		rc="$?"
	}

	[ "$rc" = 233 ] && {
		# Device might not support virtual interfaces, so the interface never got deleted in the first place.
		# Check if the interface already exists, and avoid failing in this case.
		ip link show dev "$ifname" >/dev/null 2>/dev/null && rc=0
	}

	[ "$rc" != 0 ] && wireless_setup_failed INTERFACE_CREATION_FAILED
	return $rc
}

get_uci_idx() {
	local phy=$1
	local mode=$2
	local ssid=$3

	local sections=$(seq 0 15)


	phyidx=$(echo "$phy" | sed 's/\(phy\)\([0-9]*\)/\2/g')
	device="wl${phyidx}"

	for section in $sections; do
		d=$(uci get wireless.@wifi-iface[$section].device)
		m=$(uci get wireless.@wifi-iface[$section].mode)
		s=$(uci get wireless.@wifi-iface[$section].ssid)


		[ -z "$d" ] && continue
		[ -z "$m" ] && continue

		logger -t mac80211 "[$section] $device/$mode/$ssid vs  $d/$m/$s"
		[ -z "$s" ] && [ "$mode" == "ap" ] && continue

		[ "$device" != "$d" ] && continue
		[ "$mode" != "$m" ] && continue
		[ "$ssid" != "$s" ] && [ "$mode" == "ap" ] && continue

		echo "$section"
		return
	done

	echo "unknown"
}

mac80211_prepare_vif() {
	json_select config

	json_get_vars ifname mode ssid wds powersave macaddr

	logger -t mac80211 "mac80211_prepare_vif mode $mode ifname $ifname macaddr $macaddr"

	if_idx=${if_idx:-0}
	sta_idx=${sta_idx:-0}
	ap_idx=${ap_idx:-0}

	[ -n "$ifname" ] || {
		logger -t mac80211 "$phy $mode generate ifname - if_idx $if_idx apsta $apsta"
		case $mode in
			ap)
				if [ "$apsta" = "0" ]; then
					if [ $ap_idx -eq 0 ]; then
						ifname="wl${phy#phy}"
					else
						ifname="wl${phy#phy}.${ap_idx}"
					fi
				else
					ifname="wl${phy#phy}.$(($ap_idx + 1))"
				fi
				;;
			sta)
				if [ "$apsta" = "1" ]; then
					if [ $sta_idx -eq 0 ]; then
						ifname="wl${phy#phy}"
					else
						ifname=""
						logger -t mac80211 "$phy ifname generation failed - only one STA allowed"
					fi
				else
					ifname=""
					logger -t mac80211 "$phy ifname generation - no STA allowed !apsta"
				fi
				;;
			*)
				;;
		esac

		uci_idx=$(get_uci_idx $phy $mode $ssid)
		logger -t mac80211 "$phy $mode generated ifname $ifname uci_idx $uci_idx"

		[ "$uci_idx" != "unknown" ] && {
			uci -q set wireless.@wifi-iface[$uci_idx].ifname="$ifname"
			uci commit wireless
		}
	}

	case $mode in
		ap)
			ap_idx=$(($ap_idx + 1))
			;;
		sta)
			sta_idx=$(($sta_idx + 1))
			;;
		*)
			;;
	esac

	set_default wds 0
	set_default powersave 0

	json_select ..

	[ -n "$macaddr" ] || {
		if [ "$apsta" = "1" ]; then
			macidx="$(($if_idx + 1))"
		else
			macidx="$if_idx"
		fi

		macaddr="$(mac80211_generate_mac $ifname $macidx)"
		logger -t mac80211 "$ifname using generated macaddr $macaddr"
	}

	nvram set ${ifname}_bss_enabled=1
	nvram set ${ifname}_hwaddr=${macaddr}

	if_idx=$(($if_idx + 1))

	json_add_object data
	json_add_string ifname "$ifname"
	json_close_object
	json_select config

	# It is far easier to delete and create the desired interface
	case "$mode" in
		adhoc)
			mac80211_iw_interface_add "$phy" "$ifname" adhoc || return
		;;
		ap)
			# Hostapd will handle recreating the interface and
			# subsequent virtual APs belonging to the same PHY
			if [ -n "$hostapd_ctrl" ]; then
				type=bss
			else
				type=interface
			fi

			mac80211_hostapd_setup_bss "$phy" "$ifname" "$macaddr" "$type" || return

			mac80211_iw_interface_add "$phy" "$ifname" __ap || return
			ifconfig $ifname hw ether $macaddr

			[ -n "$hostapd_ctrl" ] || {
				hostapd_ctrl="${hostapd_ctrl:-/var/run/hostapd/$ifname}"
			}

			bcm_setup_bss "$ifname"
		;;
		mesh)
			mac80211_iw_interface_add "$phy" "$ifname" mp || return
		;;
		monitor)
			mac80211_iw_interface_add "$phy" "$ifname" monitor || return
		;;
		sta)
			local wdsflag=
			staidx="$(($staidx + 1))"
			[ "$wds" -gt 0 ] && wdsflag="4addr on"
			mac80211_iw_interface_add "$phy" "$ifname" managed "$wdsflag" || return
			[ "$powersave" -gt 0 ] && powersave="on" || powersave="off"
			#iw "$ifname" set power_save "$powersave"
			bcm_setup_sta "$ifname"
		;;
	esac

	case "$mode" in
		monitor|mesh)
			[ "$auto_channel" -gt 0 ] || iw dev "$ifname" set channel "$channel" $iw_htmode
		;;
	esac

	#if [ "$mode" != "ap" ]; then
		# ALL ap functionality will be passed to hostapd
		# All interfaces must have unique mac addresses
		# which can either be explicitly set in the device
		# section, or automatically generated
		#ip link set dev "$ifname" address "$macaddr"
	#fi

	json_select ..
}

mac80211_setup_supplicant() {
	wpa_supplicant_prepare_interface "$ifname" nl80211 || return 1
	if [ "$mode" = "sta" ]; then
		wpa_supplicant_add_network "$ifname"
	else
		wpa_supplicant_add_network "$ifname" "$freq" "$htmode" "$noscan"
	fi

	# Today -H not supported - check if we need this
	#wpa_supplicant_run "$ifname" ${hostapd_ctrl:+-H $hostapd_ctrl}
	wpa_supplicant_run "$ifname"
}

mac80211_setup_supplicant_noctl() {
	wpa_supplicant_prepare_interface "$ifname" nl80211 || return 1
	wpa_supplicant_add_network "$ifname" "$freq" "$htmode" "$noscan"
	wpa_supplicant_run "$ifname"
}

mac80211_prepare_iw_htmode() {
	case "$htmode" in
		VHT20|HT20) iw_htmode=HT20;;
		HT40*|VHT40|VHT160)
			case "$hwmode" in
				a)
					case "$(( ($channel / 4) % 2 ))" in
						1) iw_htmode="HT40+" ;;
						0) iw_htmode="HT40-";;
					esac
				;;
				*)
					case "$htmode" in
						HT40+) iw_htmode="HT40+";;
						HT40-) iw_htmode="HT40-";;
						*)
							if [ "$channel" -lt 7 ]; then
								iw_htmode="HT40+"
							else
								iw_htmode="HT40-"
							fi
						;;
					esac
				;;
			esac
			[ "$auto_channel" -gt 0 ] && iw_htmode="HT40+"
		;;
		VHT80)
			iw_htmode="80MHZ"
		;;
		NONE|NOHT)
			iw_htmode="NOHT"
		;;
		*) iw_htmode="" ;;
	esac

}

mac80211_setup_adhoc() {
	json_get_vars bssid ssid key mcast_rate

	keyspec=
	[ "$auth_type" = "wep" ] && {
		set_default key 1
		case "$key" in
			[1234])
				local idx
				for idx in 1 2 3 4; do
					json_get_var ikey "key$idx"

					[ -n "$ikey" ] && {
						ikey="$(($idx - 1)):$(prepare_key_wep "$ikey")"
						[ $idx -eq $key ] && ikey="d:$ikey"
						append keyspec "$ikey"
					}
				done
			;;
			*)
				append keyspec "d:0:$(prepare_key_wep "$key")"
			;;
		esac
	}

	brstr=
	for br in $basic_rate_list; do
		wpa_supplicant_add_rate brstr "$br"
	done

	mcval=
	[ -n "$mcast_rate" ] && wpa_supplicant_add_rate mcval "$mcast_rate"

	iw dev "$ifname" ibss join "$ssid" $freq $iw_htmode fixed-freq $bssid \
		beacon-interval $beacon_int \
		${brstr:+basic-rates $brstr} \
		${mcval:+mcast-rate $mcval} \
		${keyspec:+keys $keyspec}
}

mac80211_setup_mesh() {
	json_get_vars ssid mesh_id mcast_rate

	mcval=
	[ -n "$mcast_rate" ] && wpa_supplicant_add_rate mcval "$mcast_rate"
	[ -n "$mesh_id" ] && ssid="$mesh_id"

	iw dev "$ifname" mesh join "$ssid" freq $freq $iw_htmode \
		${mcval:+mcast-rate $mcval} \
		beacon-interval $beacon_int
}

mac80211_setup_vif() {
	local name="$1"
	local failed

	json_select data
	json_get_vars ifname
	json_select ..

	json_select config
	json_get_vars mode
	json_get_var vif_txpower txpower

	ip link set dev "$ifname" up || {
		wireless_setup_vif_failed IFUP_ERROR
		json_select ..
		return
	}

	logger -t mac80211 "mac80211_setup_vif $mode $ifname"

	set_default vif_txpower "$txpower"
	#[ -z "$vif_txpower" ] || iw dev "$ifname" set txpower fixed "${vif_txpower%%.*}00"

	case "$mode" in
		mesh)
			wireless_vif_parse_encryption
			freq="$(get_freq "$phy" "$channel")"
			if [ "$wpa" -gt 0 -o "$auto_channel" -gt 0 ] || chan_is_dfs "$phy" "$channel"; then
				mac80211_setup_supplicant || failed=1
			else
				mac80211_setup_mesh
			fi
			for var in $MP_CONFIG_INT $MP_CONFIG_BOOL $MP_CONFIG_STRING; do
				json_get_var mp_val "$var"
				[ -n "$mp_val" ] && iw dev "$ifname" set mesh_param "$var" "$mp_val"
			done
		;;
		adhoc)
			wireless_vif_parse_encryption
			if [ "$wpa" -gt 0 -o "$auto_channel" -gt 0 ]; then
				freq="$(get_freq "$phy" "$channel")"
				mac80211_setup_supplicant_noctl || failed=1
			else
				mac80211_setup_adhoc
			fi
		;;
		sta)
			mac80211_setup_supplicant || failed=1
		;;
	esac

	json_select ..
	[ -n "$failed" ] || wireless_add_vif "$name" "$ifname"
}

mac80211_add_vif() {
	local name="$1"

	json_select data
	json_get_vars ifname
	json_select ..

	wireless_add_vif "$name" "$ifname"
}

get_freq() {
	local phy="$1"
	local chan="$2"
	iw "$phy" info | grep -E -m1 "(\* ${chan:-....} MHz${chan:+|\\[$chan\\]})" | grep MHz | awk '{print $2}'
}

chan_is_dfs() {
	local phy="$1"
	local chan="$2"
	iw "$phy" info | grep -E -m1 "(\* ${chan:-....} MHz${chan:+|\\[$chan\\]})" | grep -q "MHz.*radar detection"
	return $!
}

mac80211_interface_cleanup() {
	local phy="$1"

	for wdev in $(list_phy_interfaces "$phy"); do
		logger -t mac80211 "mac80211_interface_cleanup $wdev"
		ip link set dev "$wdev" down 2>/dev/null
		iw dev "$wdev" del

		type wlctl > /dev/null 2>&1 && {
			wlctl -i ${wdev} down
		}

	done
}

mac80211_set_noscan() {
	hostapd_noscan=1
}

drv_mac80211_cleanup() {
	hostapd_common_cleanup

	type wlctl > /dev/null 2>&1 && {
		local phy="$1"

		phyidx=$(echo "$phy" | sed 's/\(phy\)\([0-9]*\)/\2/g')
		phyiface=$(iw dev | grep phy#${phyidx} -A1 | grep Interface | cut -d' ' -f2 | cut -d. -f1)
		#echo "Bring down ${phyiface}" > /dev/console
		wlctl -i ${phyiface} down
	}
}

drv_mac80211_setup() {
	json_select config
	json_get_vars \
		phy macaddr path \
		country chanbw distance \
		txpower antenna_gain \
		rxantenna txantenna \
		frag rts beacon_int:100 htmode
	json_get_values basic_rate_list basic_rate
	json_get_vars apsta
	json_get_vars country_rev
	json_get_vars acs_exclude_dfs:1
	json_get_vars acs_refresh_period:0
	json_get_values channel_list channels
	json_get_vars legacy_rates
	json_select ..

	find_phy || {
		echo "Could not find PHY for device '$1'"
		wireless_set_retry 0
		return 1
	}

	logger -t mac8011 "mac80211_setup $phy"

	phyidx=$(echo "$phy" | sed 's/\(phy\)\([0-9]*\)/\2/g')
	phyiface=$(iw dev | grep phy#${phyidx} -A1 | grep Interface | cut -d' ' -f2 | cut -d. -f1)

	wireless_set_data phy="$phy"
	mac80211_interface_cleanup "$phy"

	# convert channel to frequency
	[ "$auto_channel" -gt 0 ] || freq="$(get_freq "$phy" "$channel")"

	wlctl -i ${phyiface} down
	wlctl -i ${phyiface} mbss 1
	wlctl -i ${phyiface} radar 1

	[ -n "$country" ] && {
		iw reg get | grep -q "^country $country:" || {
			iw reg set "$country"
			sleep 1
		}

		type wlctl > /dev/null 2>&1 && {
			if [ -n "$country_rev" ]; then
				logger -t mac80211 "$phy country $country country_rev $country_rev"
				wlctl -i ${phyiface} country "$country/$country_rev"
			else
				wlctl -i ${phyiface} country "$country"
			fi
			sleep 1
			#wlctl -i ${phyiface} up
		}
	}

	bcm_chanspec_workaround=1
	[ "$auto_channel" -gt 0 ] && {
		bcm_auto_channel "$phy" "$hwmode" "$htmode" \
			"$acs_exclude_dfs" "$acs_refresh_period" "$channel_list"
		bcm_chanspec_workaround=0
	} || killall -9 acsd2

	[ -n "$channel" ] && {
		type wlctl > /dev/null 2>&1 && {
			bcm_set_chanspec "$phyiface" "$channel" "$htmode" "$hwmode"
			orig_chanspec=$(bcm_get_chanspec "$phyiface")
			sleep 1
		}
	}

	bcm_add_radio_capabilities "$phyiface" "$htmode"

	[ -z "$apsta" ] && {
		apsta=0
		logger -t mac80211 "$phy apsta not set, assume apsta=$apsta"
	}

	[ -n "$apsta" ] && {
		type wlctl > /dev/null 2>&1 && {
			logger -t mac80211 "$phy $phyiface set apsta $apsta"

			# workaround - toggle apsta
			if [ "$apsta" = "0" ]; then
				wl -i ${phyiface} apsta 1
				wl -i ${phyiface} apsta 0
			else
				wl -i ${phyiface} apsta 0
				wl -i ${phyiface} apsta 1
			fi

			wl -i ${phyiface} roam_off 1
		}
	}

	hostapd_conf_file="/var/run/hostapd-$phy.conf"

	no_ap=1
	macidx=0
	staidx=0

	[ -n "$chanbw" ] && {
		for file in /sys/kernel/debug/ieee80211/$phy/ath9k/chanbw /sys/kernel/debug/ieee80211/$phy/ath5k/bwmode; do
			[ -f "$file" ] && echo "$chanbw" > "$file"
		done
	}

	set_default rxantenna 0xffffffff
	set_default txantenna 0xffffffff
	set_default distance 0
	set_default antenna_gain 0

	[ "$txantenna" = "all" ] && txantenna=0xffffffff
	[ "$rxantenna" = "all" ] && rxantenna=0xffffffff

	iw phy "$phy" set antenna $txantenna $rxantenna >/dev/null 2>&1
	iw phy "$phy" set antenna_gain $antenna_gain >/dev/null 2>&1
	iw phy "$phy" set distance "$distance" >/dev/null 2>&1

	[ -n "$frag" ] && iw phy "$phy" set frag "${frag%%.*}"
	[ -n "$rts" ] && iw phy "$phy" set rts "${rts%%.*}"

	has_ap=
	hostapd_ctrl=
	hostapd_noscan=
	for_each_interface "ap" mac80211_check_ap

	rm -f "$hostapd_conf_file"

	for_each_interface "sta adhoc mesh" mac80211_set_noscan
	[ -n "$has_ap" ] && mac80211_hostapd_setup_base "$phy"

	mac80211_prepare_iw_htmode
	for_each_interface "ap" mac80211_prepare_vif
	for_each_interface "sta" mac80211_prepare_vif

	[ -n "$hostapd_ctrl" ] && {
		logger -t mac80211 "start hostapd for radio $phy"
		/usr/sbin/hostapd -s -P /var/run/wifi-$phy.pid -B "$hostapd_conf_file"
		ret="$?"
		cnt=0

		until [ -f /var/run/wifi-$phy.pid ]
		do
			cnt=$(( $cnt + 1 ))
			sleep 1
			logger -t mac80211 "waiting hostapd pid - ${cnt}s"
			[ $cnt -gt 3 ] && break
		done

		wireless_add_process "$(cat /var/run/wifi-$phy.pid)" "/usr/sbin/hostapd" 1
		[ "$ret" != 0 ] && {
			wireless_setup_failed HOSTAPD_START_FAILED
			return
		}
	}


	[ -z "$legacy_rates" ] && legacy_rates=0
	[ -n "$legacy_rates" ] && bcm_set_legacy_rates "$phyiface" "$hwmode" "$legacy_rates"

	for_each_interface "sta adhoc mesh monitor" mac80211_setup_vif

	wireless_set_up

	# AP interfaces must be registered with netifd for "ifname"
	# property to appear in "ubus call network.wireless status"
	# output. Do this after wireless_set_up to work around issue
	# with netifd bringing the interface up and interfering with
	# hostapd operation.
	for_each_interface "ap" mac80211_add_vif

	if [ -n "$txpower" ]; then
		logger -t mac80211 "$phy txpower $txpower"
		wlctl -i ${phyiface} txpwr1 -d "$txpower"
	else
		logger -t mac80211 "$phy set default txpower"
		wlctl -i ${phyiface} txpwr1 -1
	fi

	brcm_service_start

	[ -n "$channel" -a "$bcm_chanspec_workaround" -eq 1 ] && {
		type wlctl > /dev/null 2>&1 && {
			# Workaround - after reboot, even we setup chanspec correctly
			# before hostapd start sometimes we see driver skip our set
			# and choose own channel. Be sure hostapd started and check
			# if we need to fix chanspec again.
			sleep 2
			cur_chanspec=$(bcm_get_chanspec "$phyiface")
			[ "$orig_chanspec" != "$cur_chanspec" ] && {
				logger -t mac80211 "different chanspec after fresh start $orig_chanspec vs $cur_chanspec - set chanspec again"
				bcm_set_chanspec "$phyiface" "$channel" "$htmode" "$hwmode"
			}

			# After reboot could be driver stuck in CAC - which base on scan internaly ...
			# Then any scan will fail. Simple abort scan after reboot.
			wl -i "$phyiface" scanabort
		}
	}

}

list_phy_interfaces() {
	local phy="$1"
	if [ -d "/sys/class/ieee80211/${phy}/device/net" ]; then
		ls "/sys/class/ieee80211/${phy}/device/net" 2>/dev/null;
	else
		ls "/sys/class/ieee80211/${phy}/device" 2>/dev/null | grep net: | sed -e 's,net:,,g'
	fi
}

drv_mac80211_teardown() {
	wireless_process_kill_all

	json_select data
	json_get_vars phy
	json_select ..

	logger -t mac80211 "mac80211_teadown $phy"
	mac80211_interface_cleanup "$phy"
}

add_driver mac80211

