
config board 'board'
	# Spec
	option hasAdsl '0'
	option hasVdsl '0'
	option hasEthWan '1'
	option hasVoice '1'
	option hasDect '0'
	option hasUsb '1'
	option hasWifi '1'
	option hasSfp '1'
	option hasCatv '1'
	# Voice
	option VoicePorts '2'
	option VoicePortNames 'Tel_2 Tel_1'
	option VoicePortOrder 'brcm0 brcm1'
	# SFP
	option sfpBus '/dev/i2c-0'
	# Ethernet
	option ethernetWanPort 'eth4'
	option ethernetPortNames 'LAN1 LAN2 LAN3 LAN4 WAN'
	option ethernetPortOrder 'eth0 eth1 eth2 eth3 eth4'
	option ethernetLanPorts 'eth0 eth1 eth2 eth3'
	# GPIO
	option sk9822_attr_file '/sys/devices/platform/canyon_led/sk9822/led_color'
	option sk9822_brightness_file '/sys/devices/platform/canyon_led/sk9822/led_brightness'

###############################################################################
#		Button config, start
############### gpio buttons, driver config

config gpio_button gpio_buttons
	list buttons gpio_dimming
	list buttons gpio_reset
	list buttons gpio_wps

config gpio_button gpio_dimming
	option addr 9
	option active low

config gpio_button gpio_reset
	option addr 12
	option active low

config gpio_button gpio_wps
	option addr 13
	option active low

############### mapping function button to driver button
config button RESET
	list button gpio_reset
	option minpress 0
	option minpress_indicator 0
	option longpress 3000
	option longpress_indicator 1
	option hotplug softresetbutton
	option hotplug_long resetbutton

config button DIMMING
	list button gpio_dimming
	option hotplug dimmingbutton
	option minpress 0
	option minpress_indicator 0

config button WPS
	list button gpio_wps
	option minpress 0
	option minpress_indicator 0
	option longpress 3000
	option longpress_indicator 0
	option hotplug wpsbutton
	option hotplug_long wifibutton

###############################################################################
#		Led config, start
#

config gpio_led gpio_leds
	list leds lan1_green
	list leds lan1_yellow
	list leds lan2_green
	list leds lan2_yellow
	list leds lan3_green
	list leds lan3_yellow
	list leds lan4_green
	list leds lan4_yellow
	list leds internet
	list leds catv_green
	list leds catv_red
	list leds uplink_green
	list leds uplink_red
	list leds voip
	list leds wifi
	list leds statusled_red
	list leds statusled_blue
	list leds statusled_green
	list leds statusled_yellow
	list leds statusled_off

config gpio_led lan1_yellow
	option active		high
	option sysfs_attr	lan1:yellow
	option mode		led_linux

config gpio_led lan1_green
	option active		high
	option sysfs_attr	lan1:green
	option mode		led_linux

config gpio_led lan2_yellow
	option active		high
	option sysfs_attr	lan2:yellow
	option mode		led_linux

config gpio_led lan2_green
	option active		high
	option sysfs_attr	lan2:green
	option mode		led_linux

config gpio_led lan3_yellow
	option active		high
	option sysfs_attr	lan3:yellow
	option mode		led_linux

config gpio_led lan3_green
	option active		high
	option sysfs_attr	lan3:green
	option mode		led_linux

config gpio_led lan4_yellow
	option active		high
	option sysfs_attr	lan4:yellow
	option mode		led_linux

config gpio_led lan4_green
	option active		high
	option sysfs_attr	lan4:green
	option mode		led_linux

config gpio_led catv_green
	option active		high
	option sysfs_attr	catv:green
	option mode		led_linux

config gpio_led catv_red
	option active		high
	option sysfs_attr	catv:red
	option mode		led_linux

config gpio_led internet
	option active		high
	option sysfs_attr	internet
	option mode		led_linux

config gpio_led uplink_green
	option active		high
	option sysfs_attr	uplink:green
	option mode		led_linux

config gpio_led uplink_red
	option active		high
	option sysfs_attr	uplink:red
	option mode		led_linux

config gpio_led voip
	option active		high
	option sysfs_attr	voip
	option mode		led_linux

config gpio_led wifi
	option active		high
	option sysfs_attr	wifi
	option mode		led_linux

# RGB LED: virtual LED for each color
config gpio_led 	statusled_red
option rgb_color	ff0000
option mode		led_sk9822

config gpio_led 	statusled_blue
option rgb_color	0000ff
option mode		led_sk9822

config gpio_led 	statusled_green
option rgb_color	006000
option mode		led_sk9822

config gpio_led 	statusled_yellow
option rgb_color	ffa000
option mode		led_sk9822

config gpio_led 	statusled_off
option rgb_color	000000
option mode		led_sk9822

############### mapping led function to driver led
config led_map led_map
	list press_indicator led_lan
	list press_indicator led_catv
	list press_indicator led_internet
	list press_indicator led_wan_speed
	list press_indicator led_voice
	list press_indicator led_wifi
	list press_indicator led_composite
	list functions lan
	list functions status
	list functions internet
	list functions upgrade
	list functions wps
	list functions wifi
	list functions catv
	list functions voice
	list functions wan_speed
	list functions wan_speed_composite
	list functions composite

config led_map led_status
config led_map led_upgrade
config led_map led_wan_speed
config led_map led_wps

config led_map led_lan
	list led_action_ok		'lan1_green = ON'
	list led_action_ok		'lan1_yellow = ON'
	list led_action_ok		'lan2_green = ON'
	list led_action_ok		'lan2_yellow = ON'
	list led_action_ok		'lan3_green = ON'
	list led_action_ok		'lan3_yellow = ON'
	list led_action_ok		'lan4_green = ON'
	list led_action_ok		'lan4_yellow = ON'
	list led_action_off		'lan1_green = OFF'
	list led_action_off		'lan1_yellow = OFF'
	list led_action_off		'lan2_green = OFF'
	list led_action_off		'lan2_yellow = OFF'
	list led_action_off		'lan3_green = OFF'
	list led_action_off		'lan3_yellow = OFF'
	list led_action_off		'lan4_green = OFF'
	list led_action_off		'lan4_yellow = OFF'

config led_map led_internet
	list led_action_ok		'internet = ON'
	list led_action_notice		'internet = FLASH_SLOW'
	list led_action_error		'internet = OFF'
	list led_action_off		'internet = OFF'

config led_map led_wifi
	list led_action_ok		'wifi = ON'
	list led_action_notice		'wifi = FLASH_SLOW'
	list led_action_off		'wifi = OFF'

config led_map led_catv
	list led_action_ok		'catv_green = ON'
	list led_action_ok		'catv_red = OFF'
	list led_action_off		'catv_green = OFF'
	list led_action_off		'catv_red = OFF'
	list led_action_notice		'catv_green = OFF'
	list led_action_notice		'catv_red = OFF'
	list led_action_alert		'catv_green = OFF'
	list led_action_alert		'catv_red = FLASH_SLOW'
	list led_action_error		'catv_green = OFF'
	list led_action_error		'catv_red = FLASH_SLOW'

config led_map led_voice
	list led_action_ok		'voip = ON'
	list led_action_notice		'voip = ON'
	list led_action_alert		'voip = FLASH_SLOW'
	list led_action_error		'voip = FLASH_SLOW'
	list led_action_off		'voip = OFF'

config led_map led_wan_speed_composite
	list led_action_ok		'uplink_green = ON'
	list led_action_ok		'uplink_red = OFF'
	list super_ok			'upgrade_off, wan_speed_ok'
	list super_ok			'upgrade_ok, wan_speed_ok'
	list super_ok			'upgrade_error, wan_speed_ok'

	list led_action_notice		'uplink_green = FLASH_SLOW'
	list led_action_notice		'uplink_red = OFF'
	list super_notice		'wan_speed_notice'
	list super_notice		'upgrade_notice'

	list led_action_error		'uplink_green = OFF'
	list led_action_error		'uplink_red = FLASH_SLOW'
	list super_error		'upgrade_ok, wan_speed_error'
	list super_error		'upgrade_off, wan_speed_error'
	list super_error		'upgrade_error, wan_speed_error'

	list led_action_off		'uplink_green = OFF'
	list led_action_off		'uplink_red = OFF'
	list super_off			'upgrade_off, wan_speed_off'
	list super_off			'upgrade_ok, wan_speed_off'
	list super_off			'upgrade_error, wan_speed_off'

config led_map led_composite
	list led_action_off		'statusled_off = ON'
	list super_off			'status_off, wps_off'

	list led_action_ok		'statusled_green = ON'
	list super_ok			'status_ok, wps_off'

	list led_action_eok		'statusled_green = FLASH_SLOW'
	list super_eok			'status_notice, wps_off'

	list led_action_alert		'statusled_red = ON'
	list super_alert		'status_error'
	list super_alert		'upgrade_error'

	list led_action_notice		'statusled_blue = ON'
	list super_notice		'status_ok, wps_ok'
	list super_notice		'status_off, wps_ok'

	list led_action_error		'statusled_blue = FLASH_SLOW'
	list super_error		'status_ok, wps_notice'
	list super_error		'status_off, wps_notice'

	list led_action_custom		'statusled_yellow = FLASH_SLOW'
	list super_custom		'upgrade_notice'
