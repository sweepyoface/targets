define Image/Build/HeaderY3
	# Args: $1=header $2=loader $3=bootfs $4=rootfs $5=metadata $6=pkginfo $7=upgrade script $8=cferam backup [$9=image format version]
	# Any unused arguments should be passed as "void"

	echo "In Image/Build/Header $(1) $(2) $(3) $(4) $(5) $(6) $(7) $(8) $(9)"

	# Generate a .y3 header. Format .y3 is pretty much backwards
	# compatible with .y2. However, enusure to keep the layout order
	# of cfe, vmlinux, ubifs, ubi, pkginfo and md5 constant for
	# backwards compatibillity! New sections can be added ONLY in
	# between pkginfo and sig (the signing)!
	# By adding &&'s the header generation become atomic.
	#
	# Iopsys image format version history:
	# .y  version 1  feeds/bcmkernel 52c73c98fd9f3  Apr 29 09:44:41 2015  Support for nvram backups in separate eraseblocks.
	# .y2 version 2  feeds/targets   0bbdb150a6ccb  Feb 9 16:10:47 2016   Generate y2 image.
	# .y2 version 2  feeds/bcmkernel 996ea07a84d77  Feb 9 15:41:26 2016   cfe get y2 support.
	# .y2 version 3  feeds/targets   bfd5ef33ff478  Jun 10 15:10:46 2016  Add arch, release and pkginfo metadata to y2, refs #9110.
	# .y2/.y3 ver 3  feeds/targets   c60866ce5dc02  May 23 13:52:35 2017  Changed flash layout; cfe ram in separate flash blocks due
	#                                                                     to large size on ARM. Experimental branch.
	# .y2 version 4  feeds/targets   f0e230e8df32a  Jun 28 16:10:34 2017  Version change for development of .y3 in experimental branch above.
	# .y3 version 5  feeds/targets   0972962d7d1f8  Jul 4 16:00:03 2017   Generate .y3 due to .y2 version 4 would brick old Iopsys 3 devices.
	#                                                                     cfe ram in separate blocks on ARM. Official branch. Adds
	#                                                                     precalculated section offsets in the header.
	# .y3 version 5  feeds/bcmkernel b3935556c0700  Sep 4 14:44:11 2017   cfe can upgrade from .y2 to .y3
	# .y3 version 6  feeds/targets   9063b7cb1ff0b  Sep 3 14:11:36 2019   Changed flash layout; single UBI pureUBI with small bootloader
	#                                                                     partition (16 blocks).
	# .y3 version 7  feeds/targets   4add2c055d332  Mar 8 13:17:02 2020   Changed flash layout; single UBI pureUBI with large bootloader
	#                                                                     partition (32 blocks). To prepare for U-boot from Broadcom in 5.04.x
	# .y3 version 8  feeds/targets   2020ac6f8c027  Mar 22 01:46:59 2020  Added feature flags. Works just like flags in /proc/cpuinfo.
	truncate --size 0 "$(KDIR)/void"
	loader_ofs=1024								&& \
	loader_sz=$$$$(find $(KDIR)/$(strip $(2)) -printf "%s")	    		&& \
	bootfs_ofs=$$$$(( loader_ofs + loader_sz ))					&& \
	bootfs_sz=$$$$(find $(KDIR)/$(strip $(3)) -printf "%s")			&& \
	rootfs_ofs=$$$$(( bootfs_ofs + bootfs_sz ))					&& \
	rootfs_sz=$$$$(find $(KDIR)/$(strip $(4)) -printf "%s")			&& \
	metadata_ofs=$$$$(( rootfs_ofs + rootfs_sz ))					&& \
	metadata_sz=$$$$(find $(KDIR)/$(strip $(5)) -printf "%s")			&& \
	pkg_ofs=$$$$(( metadata_ofs + metadata_sz ))					&& \
	pkg_sz=$$$$(find $(KDIR)/$(strip $(6)) -printf "%s")			&& \
	scr_ofs=$$$$(( pkg_ofs + pkg_sz ))					&& \
	scr_sz=$$$$(find $(KDIR)/$(strip $(7)) -printf "%s")			&& \
	sig_ofs=$$$$(( scr_ofs + scr_sz ))					&& \
	sig_sz=$(if $(CONFIG_SMIMEOPT),256,0)					&& \
	md5_ofs=$$$$(( sig_ofs + sig_sz ))					&& \
	md5_sz=32								&& \
	filesize=$$$$(( md5_ofs + md5_sz ))					&& \
	echo "IntenoIopY" >$(KDIR)/hdr						&& \
	$(if $(strip $(9)), y3_version="$(strip $(9))", y3_version=8)		&& \
	echo "version $$$$y3_version" >> $(KDIR)/hdr				&& \
	echo "integrity MD5SUM" >> $(KDIR)/hdr					&& \
	echo "git $(GIT_SHORT)" >>$(KDIR)/hdr					&& \
	echo "chip $(CONFIG_BCM_CHIP_ID)" >>$(KDIR)/hdr				&& \
	echo "arch all $(CONFIG_TARGET_ARCH_PACKAGES)" >>$(KDIR)/hdr		&& \
	echo "model $(CONFIG_TARGET_FAMILY)" >>$(KDIR)/hdr			&& \
	echo "release $(IOPSYS_VERSION)" >>$(KDIR)/hdr				&& \
	echo "customer $(CONFIG_TARGET_CUSTOMER)" >>$(KDIR)/hdr			&& \
	echo "loaderofs $$$$loader_ofs" >>$(KDIR)/hdr					&& \
	echo "loader $$$$loader_sz" >>$(KDIR)/hdr					&& \
	echo "bootfsofs $$$$bootfs_ofs" >>$(KDIR)/hdr				&& \
	echo "bootfs $$$$bootfs_sz" >>$(KDIR)/hdr					&& \
	echo "rootfsofs $$$$rootfs_ofs" >>$(KDIR)/hdr				&& \
	echo "rootfs $$$$rootfs_sz" >>$(KDIR)/hdr					&& \
	echo "metadataofs $$$$metadata_ofs" >>$(KDIR)/hdr					&& \
	echo "metadata $$$$metadata_sz" >>$(KDIR)/hdr					&& \
	echo "pkginfoofs $$$$pkg_ofs" >>$(KDIR)/hdr				&& \
	echo "pkginfo $$$$pkg_sz" >>$(KDIR)/hdr					&& \
	echo "scrofs $$$$scr_ofs" >>$(KDIR)/hdr					&& \
	echo "scr $$$$scr_sz" >>$(KDIR)/hdr					&& \
	echo -n "flags " >>$(KDIR)/hdr						&& \
	  echo -n "singlepureubi " >>$(KDIR)/hdr				&& \
	  echo -n "bpartit32" >>$(KDIR)/hdr					&& \
	  echo "" >>$(KDIR)/hdr							&& \
	echo "sigofs $$$$sig_ofs" >>$(KDIR)/hdr					&& \
	echo "sig $$$$sig_sz" >>$(KDIR)/hdr					&& \
	echo "md5ofs $$$$md5_ofs" >>$(KDIR)/hdr					&& \
	echo "md5 $$$$md5_sz" >>$(KDIR)/hdr					&& \
	echo "size $$$$filesize" >>$(KDIR)/hdr

	cat $(KDIR)/hdr /dev/zero | head --bytes=1024 >$(KDIR)/$(1)

	# Verify header is LESS than 1kB. Last byte must be string null.
	[ $$$$(head -c 1024 $(KDIR)/$(1) | tr -d "\000" | wc -c) -lt 1024 ]
endef

# Generate Iopsys 5 to 6 migrator .y3
define Image/Image/Y3

	# loader
	cp $(BOOT_BIN_DIR)/loader_test_nand_$(CONFIG_BCM_CHIP_ID).bin $(KDIR)
	# bootfs
	cp $(BOOT_BIN_DIR)/brcm_full_linux.itb $(KDIR)
	# rootfs
	cp $(BOOT_BIN_DIR)/rootfs.squashfs $(KDIR)
	# metadata
	cp $(BOOT_BIN_DIR)/test_metadata.bin_headered $(KDIR)

	# Generate regular Iopsys .y3 header
	$(call Image/Build/HeaderY3,header.y3, \
		loader_test_nand_$(CONFIG_BCM_CHIP_ID).bin, \
		brcm_full_linux.itb, rootfs.squashfs, test_metadata.bin_headered, \
		pkginfo, bundled-migrator-script.tar.gz, void)

	# Concat header, loader, bootfs, rootfs and metadata together
	cat $(KDIR)/header.y3 \
		$(KDIR)/loader_test_nand_$(CONFIG_BCM_CHIP_ID).bin \
		$(KDIR)/brcm_full_linux.itb \
		$(KDIR)/rootfs.squashfs \
		$(KDIR)/test_metadata.bin_headered \
		$(KDIR)/pkginfo \
		$(KDIR)/bundled-migrator-script.tar.gz >$(BIN_DIR)/$(IOPSYS_BUILD_VERSION).y3

	# Attach checksum to combined image
	md5sum -b $(BIN_DIR)/$(IOPSYS_BUILD_VERSION).y3 |awk '{printf "%s",$$$$1}' \
		>>$(BIN_DIR)/$(IOPSYS_BUILD_VERSION).y3
	ln -sf $(BIN_DIR)/$(IOPSYS_BUILD_VERSION).y3 $(BIN_DIR)/last.y3

endef