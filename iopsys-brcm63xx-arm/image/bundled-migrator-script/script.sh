#!/bin/ash

source /lib/upgrade/nand.sh
source /lib/upgrade/iopsys.sh

# Extract named section of y3 img to tmp
# Name of file in tmp will be the same as section
extract_section() {
	local section_ofs section_sz
	local img="$1"
	local section="$2"

	section_ofs=$(get_section_offset $img $section)
	section_sz=$(get_section_size $img $section)
	[ $section_ofs -gt 0 -a $section_sz -gt 0 ] || return 0
	dd iflag=skip_bytes oflag=seek_bytes \
	   if="$img" bs=16k skip=$section_ofs count=$section_sz \
	   of="/tmp/$section" 2>/dev/null || return 0

	echo $section_sz
}

erase_iop5_volumes() {
	local next=$(get_flashbank_next)

	for volume in rootfs_$next kernel_0 kernel_1 \
							   METADATA METADATACOPY \
							   metadata_0 metadata_1; do
		if grep -q "$volume" /sys/class/ubi/*/name; then
			echo "Erasing $volume"
			ubirmvol "/dev/ubi0" -N "$volume"
		fi
	done
}

upgrade_ubi_volume() {
	local volume="$1"
	local size="$2"
	local name="$3"
	local id="$4"

	# Create a fresh UBI volume
	echo "Creating $name"
	ubimkvol /dev/ubi0 -N $name -n $id -t static -s $size

	# Write volume data to flash.
	ubiupdatevol /dev/ubi0_$id --size=$size /tmp/$volume
	fsync /dev/ubi0_$id
}

# Save nvram values in migrator raw ubi partition.
# These values are picked up by uboot on first boot
# and stored in uboot environment.
save_nvram() {
	names=$(ls /proc/nvram | sort)

	for name in $names; do
		# Skip values
		[ $name == bootline ] || [ $name == Bootline ] || \
			[ $name == watchdog ] || [ $name == wlanParams ] && continue
		# strip out everything but ascii from 32 to 126 decimal (tr takes octal ??)
		value=$(cat /proc/nvram/$name | tr -cd '\40-\176')
		echo -n "$name=$value" >> /tmp/nvram
		# add zero so that its an valid c string
		dd if=/dev/zero bs=1 count=1 2>/dev/null >> /tmp/nvram
	done

	# Create ethaddr and nummacaddrs with the values from nvram as
	# these are used by uboot.
	mac=$(cat /proc/nvram/BaseMacAddr)
	echo -n "ethaddr=$mac" >> /tmp/nvram
	dd if=/dev/zero bs=1 count=1 2>/dev/null >> /tmp/nvram

	nr_mac=$(cat /proc/nvram/NumMacAddrs)
	echo -n "nummacaddrs=$nr_mac" >> /tmp/nvram
	dd if=/dev/zero bs=1 count=1 2>/dev/null >> /tmp/nvram

	# add extra zero so there is a zero length string at end
	dd if=/dev/zero bs=1 count=1 2>/dev/null >> /tmp/nvram

	size=$(ls -l /tmp/nvram  | awk '{ print $5 }')
	echo $size
}

img="$1"

echo "Running iop5 to iop6 migration script"

# Write loader to nvram mtd partition.
# 0x000000000000-0x000000400000 : "nvram"
echo "Writing loader to flash"
size=$(extract_section $img loader)
if [ $size -gt 0 ]; then
	mtd -e /dev/mtd1 write /tmp/loader /dev/mtd1
else
	echo "Bad boot loader size; exiting."
	return 1
fi

# Erase all iop5 volumes except for the current
# rootfs volume and the data volume.
erase_iop5_volumes

# Volume id:s are important for metadata and
# boofs as they need to be loaded from the TPL.
# Hence, create all volumes now so that they are
# not occupied later.

echo "Extracting bootfs"
size=$(extract_section $img bootfs)
upgrade_ubi_volume bootfs $size bootfs1 3
upgrade_ubi_volume bootfs $size bootfs2 5

echo "Extracting rootfs"
size=$(extract_section $img rootfs)
upgrade_ubi_volume rootfs $size rootfs1 4
upgrade_ubi_volume rootfs $size rootfs2 6

echo "Saving nvram"
size=$(save_nvram)
upgrade_ubi_volume nvram $size migrator 10

echo "Upgrade completed; rebooting."
reboot

# Halt execution of sysupgrade, both when sourced and executed as a task.
return 1 || exit 1

