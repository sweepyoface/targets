
disable_addr_learn() {
	# Disable Address Learning on P0 and P6. Not sure why this is
	# needed.
	# ethctl phy ext 0x10 0xb 0x0000
	# ethctl phy ext 0x16 0xb 0x0000
	echo test
}

port_mapping() {
	#P1 <-> P0
	# Allow port 0 (0x10) to send to port 1
	ethctl phy ext 0x10 0x6 0x0802
	# Allow port 1 (0x11) to send to port 0
	ethctl phy ext 0x11 0x6 0x0801

	#P6 <-> P4
	# Allow port 4 (0x14) to send to port 6
	ethctl phy ext 0x14 0x6 0x0840
	# Allow port 6 (0x16) to send to port 4
	ethctl phy ext 0x16 0x6 0x0810
}

port_mapping_copper_wan() {
    # Dissallow any traffic from P6 (0x16) to any port
    ethctl phy ext 0x16 0x6 0x0800

    # Dissallow any traffic from P1 (0x11) to any port
    ethctl phy ext 0x11 0x6 0x0800

    #P0 <-> P4
    # Allow port 0 (0x10) to send to port 4
    ethctl phy ext 0x10 0x6 0x0810
    # Allow port 4 (0x14) to send to port 0
    ethctl phy ext 0x14 0x6 0x0801
}

sfp_100() {
	# Set port to 100BASE-FX
	ethctl phy ext 0x11 0x17 0x8504
	ethctl phy ext 0x11 0x17 0x8080
	ethctl phy ext 0x11 0x0 0x8

	# Activate 100BASE-FX noise filter
	# Device D (SERDES), Reg26
	ethctl phy ext 0x1c 0x19 0x2042
	ethctl phy ext 0x1c 0x18 0x95ba
}

sfp_1000() {
	# Set port to 1000BASE-X
	ethctl phy ext 0x11 0x17 0x8505
	ethctl phy ext 0x11 0x17 0x8080
	ethctl phy ext 0x11 0x0 0x9

	# Deactivate 100BASE-FX noise filter
	# Device D (SERDES), Reg26
	ethctl phy ext 0x1c 0x19 0x42
	ethctl phy ext 0x1c 0x18 0x95ba
}

activate_phys() {
	# Device D, external serdes (SFP)
	ethctl phy ext 0x1c 0x19 0x1140
	ethctl phy ext 0x1c 0x18 0x95a0

	# Device C, internal serdes (SFP)
	ethctl phy ext 0x1c 0x19 0x1140
	ethctl phy ext 0x1c 0x18 0x9580

	# Device 3, rgmii
	ethctl phy ext 0x1c 0x19 0x1140
	ethctl phy ext 0x1c 0x18 0x9460

	# Device 4, RGMII
	ethctl phy ext 0x1c 0x19 0x1140
	ethctl phy ext 0x1c 0x18 0x9480
}

port_forwarding() {
	# Activate forwarding for all ports.
	ethctl phy ext 0x10 0x4 0x7f
	ethctl phy ext 0x11 0x4 0x7f
	ethctl phy ext 0x14 0x4 0x7f
	ethctl phy ext 0x16 0x4 0x7f
}

fiber_status() {
        local BIT=$1
        local fiber_status_reg

        ethctl phy ext 0x1c 0x18 0x99b1 > /dev/null
        fiber_status_reg=$(ethctl phy ext 0x1c 0x19 | grep mii | awk '{ print $5 }')

        [ $(( fiber_status_reg & BIT )) == $BIT ] && return 1 || return 0
}

no_link() {
        local LINK=$(( 1 << 10 ))
        fiber_status $LINK
}

has_power() {
        local POWER=$(( 1 << 4 ))
        fiber_status $POWER
}

wan_sfp_mode () {
        # check if there is a sfp-rom present and return 0 if so
        # returns 2 if there is an error
        i2cget -y 0 0x50 0x0 >/dev/null 2>&1
        return $?
        # return 0
}

disable_ext_led () {
        ethctl phy ext 0x14 0x16 0x80ee > /dev/null
}

enable_ext_led () {
        ethctl phy ext 0x14 0x16 0x8022 > /dev/null
}

copper_speed100() {
        local BIT=$((1 << 8))
        local copper_status_reg
        copper_status_reg=$(ethctl phy ext 0x14 0x00 | grep mii | awk '{ print $5 }')
        [ $(( copper_status_reg & BIT )) == $BIT ] && return 0 || return 1
}

copper_speed1000() {
        local BIT=$((2 << 8))
        local copper_status_reg
        copper_status_reg=$(ethctl phy ext 0x14 0x00 | grep mii | awk '{ print $5 }')
        [ $(( copper_status_reg & BIT )) == $BIT ] && return 0 || return 1
}
