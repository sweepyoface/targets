get_base_macaddr() {
	local basemac="$(/usr/bin/kv_read BaseMacAddr)"
	echo "$basemac"
}

get_macaddr_maxsize() {
	local maxsize="$(/usr/bin/kv_read NumMacAddrs)"
	echo "$maxsize"
}

get_board_id() {
	local board_id="$(fw_printenv -n boardid)"
	echo "$board_id"
}

get_product_name() {
	local prodname="$(/usr/bin/kv_read ProdName)"
	echo "$prodname"
}

get_serial_number() {
	local serial_number="$(/usr/bin/kv_read SerialNumber)"
	case $serial_number in
		*[a-z]*|*[A-Z]*|*[0-9]*) ;;
		*) serial_number="0000000000" ;;
	esac
	echo "$serial_number"
}

get_psn() {
	local psn="$(/usr/bin/kv_read PSN)"
	case $psn in
		*[a-z]*|*[A-Z]*|*[0-9]*) ;;
		*) psn="0000000000" ;;
	esac
	echo "$psn"
}

get_variant() {
	local variant="$(/usr/bin/kv_read Variant)"
	case $variant in
		*[0-9]*) ;;
		*) variant="0" ;;
	esac
	echo "$variant"
}

get_hardware_version() {
	local hardware_version="$(/usr/bin/kv_read HV)"
	case $hardware_version in
		.[0-9]*) hardware_version="1$hardware_version" ;;
		[0-9]*) ;;
		*) hardware_version="0" ;;
	esac
	echo "$hardware_version"
}

get_wpa_key() {
	local wpa_key="$(/usr/bin/kv_read WpaKey)"
	case $wpa_key in
		*[a-z]*|*[A-Z]*|*[0-9]*) wpa_key=$(echo $wpa_key | sed 's/[ \t]*$//') ;;
		*) wpa_key="00000000" ;;
	esac
	echo "$wpa_key"
}

get_des_key() {
	local des_key="$(/usr/bin/kv_read DesKey)"
	echo "$des_key"
}

get_auth_key() {
	local auth_key="$(/usr/bin/kv_read AuthKey)"
	echo "$auth_key"
}

get_user_password() {
	local user_pass="$(/usr/bin/kv_read UserPassword)"
	echo "$user_pass"
}

get_acs_password() {
	local acs_pass="$(/usr/bin/kv_read AcsPassword)"
	echo "$acs_pass"
}

get_production_mode() {
	local production="$(/usr/bin/kv_read Production)"
	echo "$production"
}
