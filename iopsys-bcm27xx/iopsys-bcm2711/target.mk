#
# Copyright (C) 2019 OpenWrt.org
#

ARCH:=aarch64
SUBTARGET:=iopsys-bcm2711
BOARDNAME:=BCM2711 boards (64 bit)
CPU_TYPE:=cortex-a72

DEFAULT_PACKAGES += \
	cypress-firmware-43455-sdio \
	cypress-nvram-43455-sdio-rpi-4b \
	kmod-brcmfmac \
	kmod-brcmfmac-sdio \
	kmod-mmc \
	kmod-usb-net-rtl8152

define Target/Description
	Build firmware image for BCM2711 devices.
	This firmware features a 64 bit kernel.
endef
