IOPSYSWRT bcm27xx (Raspberry Pi) integration
===

This folder contains target recipes that have been backported from OpenWRT, then adapted and integrated into IOPSYSWRT. The target folder has been made IOPSYS-specific so as not to conflict with existing OpenWRT support.

Status
--

Currently, only the Raspberry Pi 4 (defined as `Device/rpi-4` in `image/Makefile`) is supported as a target.
The OpenWRT way of updating the target's firmware has been tested.

Network configuration
--
The machine can act as a router, provided there is an USB dongle with the right driver (right now, the kernel is configured to ship with the RTL8169 driver for USB-Ethernet dongles using this chipset).

Networking is configured as follows:
- By default, there is a DHCP server listening on the integrated Ethernet port for any requests, if any. From that interface, the device is reachable on address 192.168.1.1.
- If there is an USB Ethernet dongle attached to the board, it will be automatically configured to ask for an IP address with DHCP from the network.
- The kernel also ships with drivers for the wireless hardware in the device. The wireless interface can be configured either in AP or STA mode.

Compiling an IOPSYSWRT OS image and flashing it
--

To compile an image targeting the Raspberry Pi 4, type in these commands:
```
git clone https://dev.iopsys.eu/iopsys/iopsyswrt.git
cd iopsyswrt
git checkout origin/feature/rpi4
./iop setup_host
./iop bootstrap
./iop feeds_update
./iop genconfig rpi-4

make -j[nr_threads]
```

This should generate gzip-compressed flashable images in ext4 and squashfs formats in `bin/targets/iopsys-bcm27xx`. **Only the ext4 formatted image is supported at the moment** (userspace has not been adapted for a read-only root file system, notably with dropbear generating its host key).

Insert a micro-SD card, and use this command to flash an image to the SD card from the top-level iopsyswrt directory (assuming the SD card is exposed as `/dev/mmcblk0` in the system):
```
zcat bin/targets/iopsys-bcm27xx/openwrt-19.07-snapshot-6.0.0alpha0-iopsys-bcm27xx-iopsys-bcm2711-rpi-4-ext4-factory.img.gz | sudo dd of=/dev/mmcblk0 bs=2048 conv=fdatasync
```

Insert the SD card in the Raspberry Pi and power it up.

Accessing the device
---
By default, a shell is available on:
- SSH: `ssh root@192.168.1.1`, password 10pD3v
- the [serial port](https://elinux.org/RPi_Serial_Connection)
