platform_check_image() {
	return 0
}

platform_do_upgrade() {
	echo "BOARD $board"
	iopsys_do_upgrade_econet "$1"
}

# Econet upgrade
iopsys_do_upgrade_econet() {
	local image="$1"
	local next_bank_id="$(iopsys_get_next_bank_id)"
	[ -n "$next_bank_id" ] || return
	log sysupgrade "Starting upgrade to bank id $next_bank_id..."
	iopsys_process_upgrade_bundle "$image" pre_upgrade || return

	if [[ "ubifs" == "$(fw_printenv -n rootfstype)" ]]; then
	iopsys_write_fit_image_component "$image" boot       required mtd "boot${next_bank_id}"   || return
	iopsys_write_fit_image_component "$image" rootfs     required ubi "rootfs${next_bank_id}" || return
	iopsys_write_fit_image_component "$image" bootloader optional mtd bootloader              || return
	fi

	iopsys_clean_other_overlay || return

	iopsys_process_upgrade_bundle "$image" post_upgrade || return
	log sysupgrade "Finished upgrade to bank id $next_bank_id..."
	return 0
}

# Tell the bootloader to boot from flash "bank" $1 next time.
# $1: bank identifier (1 or 2)
platform_set_bootbank() {
	fw_setenv active_image "$1"
}

platform_get_bootbank() {
	fw_printenv -n active_image
}

platform_copy_config() {
	log sysupgrade "Copy config files"
	iopsys_copy_config
}
