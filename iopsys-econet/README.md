IOPSYSWRT en7562 (Econet) integration
===

This folder contains target recipes that have been backported from OpenWRT, then adapted and integrated into IOPSYSWRT. The target folder has been made IOPSYS-specific so as not to conflict with existing OpenWRT support.

Status
--

Following features are supported:

    + initramfs image
    + precompiled bootloader
    + basic flash layout with one free partition for user data
    + WiFi
    + LAN
    + ssh

the en7562 (defined as `Device/en7562` in `image/en7562.mk`) is supported as a target.
The OpenWRT way of updating the target's firmware has not been tested.

Preparing the board for first time
--

Raw boards must be prepared before booting iopsys image for first time:  
  
Dump wifi calibration data (cat /etc/Wireless/RT2860AP_AC/RT30xxEEPROM.bin | hexdump https://project.iopsys.eu/issues/4822)  

Precompiled bootloader is located in [repository](https://dev.iopsys.eu/feed/econet/-/blob/devel/dl/iopboot.bin)  

It is also copied to bin dir during the build process  
Connect UART console  
  
cp ./bin/targets/iopsys-econet/en7562/iopboot.bin ~/tftp;  
cp ./bin/targets/iopsys-econet/en7562/ubilinux ~/tftp;  
cp ./bin/targets/iopsys-econet/en7562/ubi_root.bin ~/tftp;  
  
[...]  
Log into uboot console  
ECNT> tftpboot iopboot.bin  
ECNT> flash erase 0x0 ${filesize}  
ECNT> flash write 0x0 ${filesize} ${loadaddr}  
ECNT> reset  
ECNT> tftpboot ubilinux  
ECNT> flash erase 0x100000 0xA00000  
ECNT> flash write 0x100000 ${filesize} ${loadaddr}  
ECNT> flash write 0x600000 ${filesize} ${loadaddr}  
ECNT> tftpboot ubi_root.bin  
ECNT> flash erase 0xb00000 0xA100000  
ECNT> flash write 0xb00000 ${filesize} ${loadaddr}  
ECNT> setenv rootfstype ubifs; saveenv  
ECNT> reset  
  
Network configuration
--
under construction  
<!---
The machine can act as a router, provided there is an USB dongle with the right driver (right now, the kernel is configured to ship with the RTL8169 driver for USB-Ethernet dongles using this chipset).
Networking is configured as follows:
- By default, there is a DHCP server listening on the integrated Ethernet port for any requests, if any. From that interface, the device is reachable on address 192.168.1.1.
- If there is an USB Ethernet dongle attached to the board, it will be automatically configured to ask for an IP address with DHCP from the network.
- The kernel also ships with drivers for the wireless hardware in the device. The wireless interface can be configured either in AP or STA mode.
-->

Compiling an IOPSYSWRT OS image
--

To compile an image targeting the en7562, type in these commands:
```
git clone git@dev.iopsys.eu:iopsys/iopsyswrt.git
cd iopsyswrt
./iop setup_host
./iop bootstrap
./iop feeds_update
./iop genconfig en7562

make -j[nr_threads]
```

This should generate flashable FIT image (tclinux) in `bin/targets/iopsys-econet/en7562`.  
**Only tclinux image is supported at the moment**

Flashing an IOPSYSWRT OS image
--

To flash an image from **linux console**, type in this command:

```  
scp ./bin/targets/iopsys-econet/en7562/last-ubifs.itb root@<board>:/tmp  
```
and on the \<board\> (ssh \<board\>)  
```
[...]  
root@iopsys:~# sysupgrade /tmp/last-ubifs.itb  
```

Board recovery
--
[DMS](https://project.iopsys.eu/projects/pingcom/dmsf?folder_id=268)  

Flash Layout
--

under construction  
<!---
[    4.066555] parsing <1m[bootloader]a,64m[ioplinux]a,-[free]a>  
[    4.073744] partition 2: name <free>, offset ffffffff, size ffffffff, mask flags 0  
[    4.081288] partition 1: name <ioplinux>, offset ffffffff, size 4000000, mask flags 0  
[    4.089096] partition 0: name <bootloader>, offset ffffffff, size 100000, mask flags 0  
[    4.097000] Creating 4 MTD partitions on "EN7512-SPI_NAND":  
[    4.102560] 0x000000000000-0x000000100000 : "bootloader"  
[    4.108663] 0x000000100000-0x000004100000 : "ioplinux"  
[    4.114635] 0x000004100000-0x00000de80000 : "free"  
[    4.120520] 0x00000de80000-0x00000e000000 : "art"  
-->
<!---
root@iopsys:~# cat /proc/mtd  
dev:    size   erasesize  name  
mtd0: 00100000 00020000 "bootloader"  
mtd1: 04000000 00020000 "ioplinux"  
mtd2: 09d80000 00020000 "free"  
mtd3: 00180000 00020000 "art"  
-->

Accessing the device
--

By default, a shell is available on:  
- SSH: `ssh root@192.168.1.1`, password 10pD3v  

Working with UBIFS
--

under construction  
<!---
Ubi volume preparation:  
root@iopsys:~# ubiformat /dev/mtd2  
root@iopsys:~# ubiattach -p /dev/mtd2  
root@iopsys:~# ubimkvol /dev/ubi0 -N data -s 128MiB  
root@iopsys:~# mkdir /opt  
root@iopsys:~# mount -t ubifs ubi0:data /opt  
-->
<!---
Accessing ubi volume:  
root@iopsys:~# mkdir /opt  
root@iopsys:~# ubiattach -p /dev/mtd2  
root@iopsys:~# mount -t ubifs ubi0:data /opt  
-->

