#
# Copyright (C) 2009 OpenWrt.org
#

ARCH:=arm
SUBTARGET:=en7562
BOARDNAME:=en7562 based boards
CPU_TYPE:=cortex-a7
FEATURES:=squashfs nand ramdisk ubifs jffs2_nand

KERNEL_PATCHVER:=5.4

define Target/Description
	Build firmware images for Econet en7562 ARM based boards.
endef

