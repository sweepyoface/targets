#!/bin/sh

. /lib/functions.sh
. /lib/upgrade/iopsys.sh

invocation=$1
image=$2
log_tag="$0 $1"

log() {
	logger -s -p daemon.info -t "$log_tag" -- "$@"
}

do_pre_upgrade() {
	local image=$1
	log "- pre_upgrade -"
	log "- dump u-boot env -"
	nanddump -s 0x7c000 -f ./uboot_env.bin /dev/mtd0

	if [[ "ubifs" != "$(fw_printenv -n rootfstype)" ]]; then
		log "- Squshfs detected -"
		do_squashfs_update $image
	fi
}

do_post_upgrade() {
	local image=$1
	log "- post_upgrade -"
	log "- restore uboot env -"
	flash_erase /dev/mtd0 0x7c000 0
	nandwrite -s 0x7c000 /dev/mtd0 ./uboot_env.bin
	log "- Adapt uboot env -"
	if [[ -z "$(fw_printenv iop_eco_bootargs)" ]]; then
		log "- Fixing uboot env variables -"
		fw_setenv iop_eco_bootargs "qdma_init=33 serdes_sel=2"
		fw_setenv bootcmd "run iop_set_mtdblock_\${rootfstype}; setenv bootargs \${iop_gen_bootargs} \${iop_sys_bootargs} \${iop_eco_bootargs}; run iop_read_kernel; bootm"
	fi
	if [[ "2" != "$(fw_printenv -n serdes_sel)" ]]; then
		log "- Fixing uboot serdes_sel variable -"
		fw_setenv serdes_sel 2
	fi
}

do_exit_reboot() {
	log "- exit and reboot -"
	reboot && sleep 60 ; reboot -f ; sleep 60 ; exit 1
}

do_squashfs_update() {
	local image=$1
	local next_bank_boot_mtd=mtd"$( find_mtd_index "boot2" )"
	local next_bank_rootfs_mtd=mtd"$( find_mtd_index "rootfs2" )"
	local next_bank_id="2"
	local ubicontainer_mtd=mtd"$( find_mtd_index "ubi_container" )"

	if [[ "1" != "$(fw_printenv -n active_image)" ]]; then
		next_bank_boot_mtd=mtd"$( find_mtd_index "boot1" )"
		next_bank_rootfs_mtd=mtd"$( find_mtd_index "rootfs1" )"
		next_bank_id="1"
	fi

	if [ ! -f "$image" ]; then
		log "- $image does not exist -"
		exit 1
	fi

	log "- Start squashfs bank$next_bank_id sysupgrade -"
	fdtextract -e bootloader $image -o /tmp/bootloader.bin
	if [ -f "bootloader.bin" ]; then
		log "- Upgrading bootloader -"
		flash_erase /dev/mtd0 0 0
		nandwrite -p /dev/mtd0 /tmp/bootloader.bin
		rm /tmp/bootloader.bin
	fi

	log "- Upgrading kernel -"
	fdtextract -e boot $image -o /tmp/boot.bin
	flash_erase /dev/$next_bank_boot_mtd 0 0
	nandwrite -p /dev/$next_bank_boot_mtd /tmp/boot.bin
	rm /tmp/boot.bin

	log "- Upgrading rootfs -"
	fdtextract -e squashfs $image -o /tmp/squashfs.bin
	flash_erase /dev/$next_bank_rootfs_mtd 0 0
	nandwrite -p /dev/$next_bank_rootfs_mtd /tmp/squashfs.bin
	rm /tmp/squashfs.bin

	log "- Install overlay partition if not present -"
	if [[ "1" != "$(fw_printenv -n overlay_installed)" ]]; then
		flash_erase /dev/$ubicontainer_mtd 0 0
		ubiformat /dev/$ubicontainer_mtd
		ubiattach -p /dev/$ubicontainer_mtd
		ubimkvol /dev/ubi0 -n 6 -N overlay1 -s 10MiB
		ubimkvol /dev/ubi0 -n 7 -N overlay2 -s 10MiB
		fw_setenv overlay_installed "1"
	fi
}

case $invocation in
	pre_upgrade)
		do_pre_upgrade $image
		;;
	post_upgrade)
		do_post_upgrade $image
		;;
esac

return 0
