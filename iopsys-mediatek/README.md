# iopsysWrt Mediatek

## Banana Pi

### Build

```
./iop genconfig -t iopsys_mediatek bpi_bananapi-r64
make
```


### Upgrade

#### the ugly way

Get a sd card Download

https://drive.google.com/drive/folders/1EK6fkGivZB3OmY38W8gN4rFAsnInMlNl

both of the images

We also need a preloader

https://drive.google.com/file/d/1Fy__GpNSWRcITEmzH4Z_jxnjrCS3BpQJ/view


follow
http://wiki.banana-pi.org/Getting_Started_with_R64#How_to_burn_image_to_SD_card


and add the SD image to a SD card

then scp or add the emmc image to a usb stick and get it to the Banana pi

then do http://wiki.banana-pi.org/Getting_Started_with_R64#How_to_burn_image_to_onboard_eMMC


Now you have a banana pi with openwrt on the emmc flash 

reboot without the sdcard 

and scp or tranfer via usb the iopsys initramfs-kernel.bin that you have built.

and then write it to emmc

dd if=/mnt/openwrt-21.02-snapshot-6.2.0alpha2-iopsys-mediatek-mt7622-bpi_bananapi-r64-initramfs-kernel.bin /dev/mmcblk0p5


reboot you should now have iopsys on your Banana pi
