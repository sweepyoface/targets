get_base_macaddr() {
	local basemac="$(cat /sys/class/net/eth0/address)"
	echo "$basemac"
}

get_macaddr_maxsize() {
	local maxsize="16"
	echo "$maxsize"
}

get_board_id() {
	local board_id="ARMVIRT"
	echo "$board_id"
}

get_product_name() {
	local prodname="iopsysWrt"
	echo "$prodname"
}

get_serial_number() {
	local serial_number="00000000"
	echo "$serial_number"
}

get_psn() {
	local psn="00000000"
	echo "$psn"
}

get_variant() {
	local variant="1"
	echo "$variant"
}

get_hardware_version() {
	local hardware_version="1.0"
	echo "$hardware_version"
}

get_wpa_key() {
	local wpa_key="00000000"
	echo "$wpa_key"
}

get_des_key() {
	local des_key="00000000"
	echo "$des_key"
}

get_auth_key() {
	local auth_key="00000000"
	echo "$auth_key"
}

get_user_password() {
	local user_pass="00000000"
	echo "$user_pass"
}

get_acs_password() {
	local acs_pass="00000000"
	echo "$acs_pass"
}

get_production_mode() {
	local production="0"
	echo "$production"
}
