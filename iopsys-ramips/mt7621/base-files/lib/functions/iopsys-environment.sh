get_base_macaddr() {
	local basemac="$(fw_printenv -n ethaddr | tr ' ' ':' | sed 's/:$//' | tr 'a-z' 'A-Z')"
	echo "$basemac"
}

get_macaddr_maxsize() {
	local maxsize="$(fw_printenv -n num_mac_addr)"
	echo "$maxsize"
}

get_board_id() {
	local board_id="" # "$(fw_printenv -n board_id)" # Returns wrong data
	[ -n "$board_id" ] || board_id="$(grep 'machine' /proc/cpuinfo  | awk '{print$3}')"
	[ -n "$board_id" ] && board_id="$(echo $board_id | tr 'a-z' 'A-Z')"
	echo "$board_id"
}

get_product_name() {
	local prodname="$(fw_printenv -n prodname)"
	echo "$prodname"
}

get_serial_number() {
	local serial_number="$(fw_printenv -n serial_number)"
	case $serial_number in
		*[a-z]*|*[A-Z]*|*[0-9]*) ;;
		*) serial_number="0000000000" ;;
	esac
	echo "$serial_number"
}

get_psn() {
	local psn="$(fw_printenv -n psn)"
	case $psn in
		*[a-z]*|*[A-Z]*|*[0-9]*) ;;
		*) psn="0000000000" ;;
	esac
	echo "$psn"
}

get_variant() {
	local variant="$(fw_printenv -n variant)"
	echo "$variant"
}

get_hardware_version() {
	local hardware_version="$(fw_printenv -n hw_version)"
	echo "$hardware_version"
}

get_wpa_key() {
	local wpa_key="$(fw_printenv -n wpa_key)"
	case $wpa_key in
		*[a-z]*|*[A-Z]*|*[0-9]*) wpa_key=$(echo $wpa_key | sed 's/[ \t]*$//') ;;
		*) wpa_key="00000000" ;;
	esac
	echo "$wpa_key"

}

get_des_key() {
	local des_key="$(fw_printenv -n des_key)"
	echo "$des_key"
}

get_auth_key() {
	local auth_key="$(fw_printenv -n auth_key)"
	echo "$auth_key"
}

get_user_password() {
	local user_pass="$(fw_printenv -n user_passwd)"
	echo "$user_pass"
}

get_acs_password() {
	local acs_pass="$(fw_printenv -n acs_password)"
	echo "$acs_pass"
}

get_production_mode() {
	local production="$(fw_printenv -n production)"
	echo "$production"
}

get_soc_vendor() {
	echo "intel"
}

get_soc_model() {
	local soc_model="$(awk '$0 ~ /^system type.*/ {printf "%s",toupper($4); exit}' /proc/cpuinfo)"
	echo "$soc_model"
}

get_soc_architecture() {
	local soc_arch="$(cat /proc/cpuinfo | grep 'cpu model' -m1 | awk '{print $4}')"
	echo "$soc_arch"
}
