#!/bin/ash
set -eu -o pipefail

# This script is run as part of sysupgrade.
# It is used for upgrading from an y3-image on IOPSYS 5 (or early IOPSYS 6)
# to IOPSYS 6 with new flash layout and U-Boot


IMAGE="$1"
LOG_TAG="$0"

log() {
	logger -s -p daemon.info -t "$LOG_TAG" -- "$@"
}

handle_exit() {
	local err="$?"
	if [ "$err" != 0 ] ; then
		log "Error $err occurred during migration to IOPSYSWRT 6. If possible, do not switch off device and ask for help."
	fi
	return "$err"
}

trap 'handle_exit' EXIT


ext_cmd() {
	local ret=0
	log "## Executing         " "$@" "..."
	"$@" | log || ret=$?
	log "## Finished executing" "$@" "with return code $ret."
	return "$ret"
}

#--------------------------------------------------------------
get_iop_tag_val() {
	local from="$1"
	local tag="$2"
	local val

	[ -r "$from" ] && val=$(head -c 1024 "$from" | strings | \
		awk "/^$tag / {print \$2}")
	[ -z "$val" ] && val=0
	echo $val
}


#--------------------------------------------------------------
# Returns byte size of a .y2 named section
# where image header is of version 3.
get_section_size_v3() {
	local from="$1"																# Image file
	local sec="$2"																# Named section
	local sz

	sz="$(get_iop_tag_val $from $sec)"
	[ -z "$sz" ] && sz=0
	echo -n $sz
}



#--------------------------------------------------------------
# Returns byte offset in a .y2 image to a named section
# where image header is of version 3.
get_section_offset_v3() {
	local cfe_ofs cfe_sz k_ofs k_sz ubifs_ofs ubifs_sz
	local ubi_ofs ubi_sz pkg_ofs pkg_sz
	local from="$1"																# Image file
	local sec="$2"																# Named section

	cfe_ofs=1024
	cfe_sz=$(get_section_size_v3 $from cfe)
	k_ofs=$((cfe_ofs + cfe_sz))
	k_sz=$(get_section_size_v3 $from vmlinux)
	ubifs_ofs=$((k_ofs + k_sz))
	ubifs_sz=$(get_section_size_v3 $from ubifs)
	ubi_ofs=$((ubifs_ofs + ubifs_sz))
	ubi_sz=$(get_section_size_v3 $from ubi)
	pkg_ofs=$((ubi_ofs + ubi_sz))
	pkg_sz=$(get_section_size_v3 $from pkginfo)

	if [ "$sec" = "cfe" ]; then
		echo -n ${cfe_ofs}
	elif [ "$sec" = "vmlinux" ]; then
		echo -n ${k_ofs}
	elif [ "$sec" = "ubifs" ]; then
		echo -n ${ubifs_ofs}
	elif [ "$sec" = "ubi" ]; then
		echo -n ${ubi_ofs}
	elif [ "$sec" = "pkginfo" ]; then
		echo -n ${pkg_ofs}
	else
		echo -n 0
	fi
}



#--------------------------------------------------------------
# Returns byte offset in a .y3 image to a named section
# where image header is of version 5.
get_section_offset_v5() {
	local ofs filesize
	local from="$1"																# Image file
	local sec="$2"																# Named section

	ofs="$(get_iop_tag_val $from ${sec}ofs)"
	filesize="$(get_iop_tag_val $from size)"
	[ -n "$ofs" ] || ofs=0
	[ -n "$filesize" ] || filesize=0
	[ $ofs -ge 1024 -a $ofs -lt $filesize ] || ofs=0							# Verify tag is less than file size
	echo -n $ofs
}



#--------------------------------------------------------------
# Returns byte size of a .y2/.y3 image named section.
get_section_size() {
	local from="$1"																# Image file
	local ver

	ver=$(get_iop_tag_val "$from" version)
	if [ $ver -ge 3 -a $ver -le 8 ]; then										# Version 4 differs from 3 in having a larger cfe.
		get_section_size_v3 "$@" || return										# Version 5 appear in .y3 and have section offsets.
	else
		echo -n 0
	fi
}



#--------------------------------------------------------------
# Returns byte offset in a .y2/.y3 image to a named section.
get_section_offset() {
	local from="$1"																# Image file
	local ver

	ver=$(get_iop_tag_val "$from" version)
	if [ $ver -ge 3 -a $ver -le 4 ]; then										# Version 4 differs from 3 in having a larger cfe.
		get_section_offset_v3 "$@" || return
	elif [ $ver -ge 5 -a $ver -le 8 ]; then										# Version 5 appear in .y3 and have section offsets.
		get_section_offset_v5 "$@" || return
	else
		echo -n 0
	fi
}



#--------------------------------------------------------------
# Find which flash "bank" we are currently running from.
# Returns a 0 or 1 string integer. Uses mostly shell built
# in functions for speed.
get_flashbank_current() {
	local cmdline mount opt uuid

	read -t 5 -s cmdline <"/proc/cmdline"

	for opt in $cmdline; do
		case $opt in
			root=ubi*rootfs_0)
				echo -n 0
				return
				;;
			root=ubi*rootfs_1)
				echo -n 1
				return
				;;
			root=PARTUUID=*)
				uuid="${cmdline##*root=PARTUUID=}"
				uuid="${uuid%%[[:space:]]*}"
				[ -n "$uuid" ] || return
				[ -x "/sbin/blkid" -o -x "/usr/sbin/blkid" ] || return

				for opt in $(blkid -c /dev/null -t "PARTUUID=${uuid}"); do
					case $opt in
						PARTLABEL=\"root0\")
							echo -n 0
							return
							;;
						PARTLABEL=\"root1\")
							echo -n 1
							return
							;;
					esac
				done
				return
				;;
		esac
	done

	while read -t 5 -s mount; do
		case $mount in
			ubi?rootfs_0*ubifs*)
				echo -n 0
				return
				;;
			ubi?rootfs_1*ubifs*)
				echo -n 1
				return
				;;
		esac
	done <"/proc/mounts"
}



#--------------------------------------------------------------
# Find which flash "bank" is pending for upgrade.
# Returns a 0 or 1 string integer.
get_flashbank_next() {
	local curr
	curr=$(get_flashbank_current) || return
	[ -n "$curr" ] || return
	echo -n $(( (curr + 1) % 2))
}



write_section_to_ubi() {
	local image="$1"
	local section="$2"
	local ubi_volume_name="$3"
	local section_ofs="$(get_section_offset "$image" "$section")"
	local section_sz="$(get_section_size "$image" "$section")"
	[ "$section_sz" -ne 0 ]  && [ -n "$section_sz" ]  || return 1
	[ "$section_ofs" -ne 0 ] && [ -n "$section_ofs" ] || return 1
	# convert from ubi volume name to ubi id number
	ubi_volume_id=$(ubinfo -d 0 -N $ubi_volume_name | grep "Volume ID:" |awk '{print $3}')
	[ -n "$ubi_volume_id" ] || return 1

	log "Writing section $section to UBI volume $ubi_volume_name..."
	grep -q ubi0_$ubi_volume_id /proc/mounts && umount -f "/dev/ubi0_$ubi_volume_id"
	
	ext_cmd ubiupdatevol "/dev/ubi0_$ubi_volume_id" -t || return
	ext_cmd ubirsvol /dev/ubi0 -N "$ubi_volume_name" -s "$section_sz" || return
	ext_cmd ubiupdatevol "/dev/ubi0_$ubi_volume_id" --size="$section_sz" \
		--skip="$section_ofs" "$image" || return
}


write_section_to_mtd() {
	local image="$1"
	local section="$2"
	local mtd_device="$3"
	local section_ofs="$(get_section_offset "$image" "$section")"
	local section_sz="$(get_section_size "$image" "$section")"
	[ "$section_sz" -ne 0 ]  && [ -n "$section_sz" ]  || return 1
	[ "$section_ofs" -ne 0 ] && [ -n "$section_ofs" ] || return 1

	log "Writing section $section to MTD device $mtd_device..."
	ext_cmd mtd erase "$mtd_device"
	dd bs=1 if="$image" skip="$section_ofs" count="$section_sz" | mtd write - "$mtd_device" | log
}


update_uboot_env() {
	local active_image="$1"
	(
		echo "active_image          $active_image"
		echo "mtdparts              mtdparts=MT7621-NAND:1M(u-boot),-(ubi)"   # switch from uboot to u-boot for consistency
		echo 'bootcmd               rescue; run nandboot'
		echo 'rootfstype' # no longer needed, because we autodetect it
		echo 'nandboot              run nandargs; ubi read ${loadaddr} boot${active_image}; bootm ${loadaddr} -'
		echo 'nandargs              run set_rootfs_args && setenv bootargs console=ttyS0,${baudrate} ${mtdparts} ubi.mtd=ubi ${rootfs_args} ${extra}'
		echo 'set_rootfs_args_ubifs rootfs_args="rootfstype=ubifs root=ubi0:rootfs${active_image}'
	) | fw_setenv --script - | log
	# --script mode does not support newlines in the var,
	# so we need to write the env two times more for the sake of readability
	fw_setenv set_rootfs_args_squashfs '
  setexpr rootfs_vol_id ${active_image} + 3
  rootfs_args="ubi.block=0,rootfs${active_image} rootfstype=squashfs root=/dev/ubiblock0_${rootfs_vol_id}"
  env delete rootfs_vol_id' | log
	fw_setenv set_rootfs_args '
  ubi read ${loadaddr} rootfs${active_image} 4 && \
  if itest.l *${loadaddr} == 73717368 ; then
    echo Detected squashfs.
    run set_rootfs_args_squashfs
  else
    echo Did not detect squashfs, assuming UBIFS.
    run set_rootfs_args_ubifs
  fi' | log
}

ubi_vol_with_id_and_name_exists() {
	local id="$1"
	local name="$2"

	[ "$(cat /sys/class/ubi/ubi0_$id/name 2>/dev/null || true)" = "$name" ] || return
}

migrate() {
	local image="$1"
	set -eu -o pipefail
	# old => old flash layout (rootfs_0, rootfs_1)
	# new => new flash layout (rootfs1, rootfs2)
	local old_current_flashbank_id="$(get_flashbank_current)"
	# spell it out explicitly, rather than doing magic calculations
	case "$old_current_flashbank_id" in
		0)
			local old_current_rootfs="rootfs_0"
			local    old_next_rootfs="rootfs_1"

			local  new_current_rootfs="rootfs1"
			local      new_current_boot="boot1"

			local     new_next_rootfs="rootfs2"
			local         new_next_boot="boot2"
			local     new_next_flashbank_id="2"
			;;
		1)
			local old_current_rootfs="rootfs_1"
			local    old_next_rootfs="rootfs_0"

			local  new_current_rootfs="rootfs2"
			local      new_current_boot="boot2"

			local     new_next_rootfs="rootfs1"
			local         new_next_boot="boot1"
			local     new_next_flashbank_id="1"
			;;
		*)
			log "Unexpected bank id $old_current_flashbank_id"
			return 1
			;;
	esac
	log "                 OLD        NEW:"
	log "current_rootfs   $old_current_rootfs   $new_current_rootfs"
	log "current_boot     -          $new_current_boot"
	log "next_rootfs      $old_next_rootfs   $new_next_rootfs"
	log "next_boot        -          $new_next_boot"

	# Initially we wanted to rename rootfs_0/1 to rootfs1/2, which is more
	# robust in case of failure in the middle, because it is still easy to
	# boot the old or the new way this way.
	# To keep consistency with the agreed on layout in terms of UBI volume ids,
	# we rename instead rootfs_0/1 to boot1/2.

	log "Updating inactive volume name $old_next_rootfs to $new_next_boot..."
	ext_cmd ubirename /dev/ubi0 "$old_next_rootfs" "$new_next_boot"

	log "Writing boot..."
	write_section_to_ubi "$image" boot_ubi "$new_next_boot"

	log "Creating new volumes for rootfs..."
	ubi_vol_with_id_and_name_exists 4 rootfs1 || ext_cmd ubimkvol /dev/ubi0 -N rootfs1 -n 4 -t dynamic -S 1     # size adjusted once filled with data
	ubi_vol_with_id_and_name_exists 5 rootfs2 || ext_cmd ubimkvol /dev/ubi0 -N rootfs2 -n 5 -t dynamic -S 1     # size adjusted once filled with data
	log "Creating new volumes for overlay..."
	ubi_vol_with_id_and_name_exists 6 overlay1 || ext_cmd ubimkvol /dev/ubi0 -N overlay1 -n 6 -t dynamic -S 300
	ubi_vol_with_id_and_name_exists 7 overlay2 || ext_cmd ubimkvol /dev/ubi0 -N overlay2 -n 7 -t dynamic -S 300
	log "All volumes created."

	log "Writing rootfs..."
	write_section_to_ubi "$image" ubifs "$new_next_rootfs"

	[ "$(cat /sys/class/mtd/mtd0/name)" == "uboot" ] || [ "$(cat /sys/class/mtd/mtd0/name)" == "u-boot" ]
	log "Writing loader... (if things fail here, do not switch off device)"
	write_section_to_mtd "$image" loader /dev/mtd0
	log "Loader written successfully"

	
	log "Printing old U-Boot environment"
	ext_cmd fw_printenv
	log "Updating U-Boot environment"
	update_uboot_env "$new_next_flashbank_id"
	log "U-Boot environment updated successfully"
	log "Printing new U-Boot environment"
	ext_cmd fw_printenv

	log "Updating currently used rootfs volume name $old_current_rootfs to $new_current_boot (will be overwritten by next upgrade)..."
	ext_cmd ubirename /dev/ubi0 "$old_current_rootfs" "$new_current_boot"

	log "Success"
	log "Rebooting."
	ext_cmd sync
	ext_cmd reboot
	ext_cmd sleep 5
	ext_cmd reboot -f
}

migrate "$IMAGE"
return 1 || exit 1 # Don't proceed with normal sysupgrade
