IOPSYS_VERSION_SUFFIX:=-X-$(CONFIG_TARGET_CUSTOMER)-$(CONFIG_TARGET_VERSION)-$(shell date '+%y%m%d_%H%M')
IOPSYS_BUILD_VERSION_SUFFIX:=$(shell echo $(IOPSYS_VERSION_SUFFIX) | sed s/\"//g)
GIT_VER=$(shell cd $(TOPDIR);git rev-parse HEAD)
GIT_SHORT:=$(shell cd $(TOPDIR);git rev-parse --short HEAD)

define Device/IopsysCommon
  IMAGE_NAME = $(1)$(IOPSYS_BUILD_VERSION_SUFFIX).$$(2)
  KERNEL := kernel-bin
  KERNEL_INSTALL :=
  IMAGES := y3
  IMAGE/y3 := iopsys_y3_image
  FILESYSTEMS := ubifs
  IOPSYS_BUILD_VERSION = $(1)$(IOPSYS_BUILD_VERSION_SUFFIX)
endef
DEVICE_VARS += IOPSYS_BUILD_VERSION

define MakeHeaderY3
	# Args: $1=header $2=cfe $3=kernel $4=ubifs $5=ubi $6=pkginfo $7=upgrade script
	# Any unused arguments should be passed as "void"

	@echo "MakeHeaderY3 $(1) $(2) $(3) $(4) $(5) $(6) $(7)"

	# Generate a .y3 header. Format .y3 is pretty much backwards
	# compatible with .y2. However, enusure to keep the layout order
	# of cfe, vmlinux, ubifs, ubi, pkginfo and md5 constant for
	# backwards compatibillity! New sections can be added only in
	# between pkginfo and sig (the signing). .y3 extends .y2 by
	# adding precalculated section offsets in the header.
	truncate --size 0 "$(KDIR)/void"
	ubifs_ofs=1024								&& \
	ubifs_sz=$$(find $(strip $(4)) -printf "%s")				&& \
	pkg_ofs=$$(( ubifs_ofs + ubifs_sz ))					&& \
	pkg_sz=$$(find $(KDIR)/$(strip $(6)) -printf "%s")			&& \
	scr_ofs=$$(( pkg_ofs + pkg_sz ))					&& \
	scr_sz=$$(find $(strip $(7)) -printf "%s")				&& \
	sig_ofs=$$(( scr_ofs + scr_sz ))					&& \
	sig_sz=$(if $(CONFIG_SMIMEOPT),256,0)					&& \
	md5_ofs=$$(( sig_ofs + sig_sz ))					&& \
	md5_sz=32								&& \
	filesize=$$(( md5_ofs + md5_sz ))					&& \
	echo "IntenoIopY" >$(KDIR)/hdr						&& \
	echo "version 5" >>$(KDIR)/hdr						&& \
	echo "integrity MD5SUM" >> $(KDIR)/hdr					&& \
	echo "git $(GIT_SHORT)" >>$(KDIR)/hdr					&& \
	echo "board $(DEVICE_NAME)" >>$(KDIR)/hdr				&& \
	echo "chip $(CONFIG_INTEL_MIPS_CHIP_ID)" >>$(KDIR)/hdr			&& \
	echo "arch all $(CONFIG_TARGET_ARCH_PACKAGES)" >>$(KDIR)/hdr		&& \
	echo "model $(DEVICE_NAME)" >>$(KDIR)/hdr				&& \
	echo "release $(IOPSYS_BUILD_VERSION)" >>$(KDIR)/hdr			&& \
	echo "customer $(CONFIG_TARGET_CUSTOMER)" >>$(KDIR)/hdr			&& \
	echo "ubifsofs $$ubifs_ofs" >>$(KDIR)/hdr				&& \
	echo "ubifs $$ubifs_sz" >>$(KDIR)/hdr					&& \
	echo "pkginfoofs $$pkg_ofs" >>$(KDIR)/hdr				&& \
	echo "pkginfo $$pkg_sz" >>$(KDIR)/hdr					&& \
	echo "scrofs $$scr_ofs" >>$(KDIR)/hdr					&& \
	echo "scr $$scr_sz" >>$(KDIR)/hdr					&& \
	echo "sigofs $$sig_ofs" >>$(KDIR)/hdr					&& \
	echo "sig $$sig_sz" >>$(KDIR)/hdr					&& \
	echo "md5ofs $$md5_ofs" >>$(KDIR)/hdr					&& \
	echo "md5 $$md5_sz" >>$(KDIR)/hdr					&& \
	echo "size $$filesize" >>$(KDIR)/hdr

	cat $(KDIR)/hdr /dev/zero | head --bytes=1024 >$(KDIR)/$(1)
endef

define Production_image
	# arg1 = erase block size
	# arg2 = rootfs ubi
	# padd boot loader
	cp $(BIN_DIR)/uboot.img $(BIN_DIR)/uboot.img.padded_to_erase_block_size

	ERASE_BLOCK_SIZE=$1                                                     && \
	echo $$ERASE_BLOCK_SIZE                                                 && \
        UBOOT_SIZE=`stat --printf="%s" $(BIN_DIR)/uboot.img.padded_to_erase_block_size`    && \
        PADDED_UBOOT_SIZE=$$(( ($$UBOOT_SIZE/$$ERASE_BLOCK_SIZE+1)*$$ERASE_BLOCK_SIZE ))   && \
	dd if=/dev/null of=$(BIN_DIR)/uboot.img.padded_to_erase_block_size bs=1 count=0 seek=$$PADDED_UBOOT_SIZE

	# build environment volume
	cp $(PLATFORM_DIR)/$(SUBTARGET)/environment.ini $(BIN_DIR)/environment.ini
	$(BUILD_DIR_HOST)/u-boot-2014.10/tools/mkenvimage -s 126976 -b -r -o $(BIN_DIR)/iopsys_env.vol $(BIN_DIR)/environment.ini
	# build ubi
	echo $2
	echo "SUBTARGET $(SUBTARGET)"
	echo "TARGET $(TARGET)"
	echo "Platform $(PLATFORM_DIR)"
	echo "TARGET_DIR $(TARGET_DIR)"
	echo $(CONFIG_TARGET)
	echo "CTD $(CONFIG_TARGET_DIR)"
	echo $(DEVICE_NAME)
	echo $(BUILD_DIR)
	echo $(BUILD_DIR_HOST)

	# not really optimal :( the naming scheme is a mess
	cp $(PLATFORM_DIR)/$(SUBTARGET)/cfg.ini $(BIN_DIR)/
	cp $2 $(BIN_DIR)/rootfs.ubifs
	# fill in correct image names in cfg.ini
	sed -i 's/|ROOTFS_IMAGE|/rootfs.ubifs/g' $(BIN_DIR)/cfg.ini
	sed -i 's/|ENV_IMAGE|/iopsys_env.vol/g' $(BIN_DIR)/cfg.ini


	cd $(BIN_DIR) ; $(STAGING_DIR_HOST)/bin/ubinize -vv -o $(BIN_DIR)/system_part.ubi -p 128KiB -m 2048 -s 2048 $(BIN_DIR)/cfg.ini
	cd $(BIN_DIR) ; $(STAGING_DIR_HOST)/bin/ltq-nand -i $(BIN_DIR)/system_part.ubi -o $(BIN_DIR)/system_part.ubi.ecc -b 131072 -p 2048 -s 64 -e 4
	cd $(BIN_DIR) ; $(STAGING_DIR_HOST)/bin/ltq-nand -i $(BIN_DIR)/uboot.img.padded_to_erase_block_size -o $(BIN_DIR)/uboot.img.ubi.ecc -b 131072 -p 2048 -s 64 -e 4

	echo "production image generation"
endef

define Build/iopsys_y3_image
	@echo
	@echo Creating y3 image for $(DEVICE_NAME)

	# make sure provided rootfs is ubifs, otherwise fails
	echo $$(basename $(IMAGE_ROOTFS)) | grep ".ubifs" -q

	# extract package info
	cat $(KDIR)/target-dir-$(ROOTFS_ID/$(DEVICE_NAME))/usr/lib/opkg/status \
	   | awk '/^Package:/ {printf "%s ",$$2} /^Version:/ {printf "%s\n",$$2}' \
	   | sort \
	   | awk '{printf "===Package start===\nPackage: %s\nVersion: %s\n",$$1,$$2}' \
	   > $(KDIR)/pkginfo
	
	$(call MakeHeaderY3,header.y3,void,void,$(IMAGE_ROOTFS),void,pkginfo,/dev/null)
	cat $(KDIR)/header.y3 $(IMAGE_ROOTFS) $(KDIR)/pkginfo > $@

	# If build is signed also sign the cfe,kernel,fs part of the image.
	$(if $(CONFIG_SMIMEOPT), \
			cat  $(IMAGE_ROOTFS) | \
			openssl dgst -sha256 -sign $(CONFIG_OPKGSMIME_KEY) \
			-passin file:$(call qstrip,$(CONFIG_OPKGSMIME_PASSFILE)) >> $@ \
	)

	# Attach checksum to combined image (.y3)
	md5sum $@ |awk '{printf "%s",$$1}' >> $@

	# prepare the link last.y3
	ln -sf $$(basename $@) $(BIN_DIR)/last.y3

	echo "production image check"
	# production image check
	$(if $(CONFIG_TARGET_IMAGE_PRODUCTION), $(call Production_image,131072,$(IMAGE_ROOTFS)) )
	$(if $(CONFIG_TARGET_IMAGE_PRODUCTION), echo $(CONFIG_TARGET_IMAGE_PRODUCTION))
	echo "p done"
endef

define InstallExtraFiles
	if [ -d ../$(shell echo $(DEVICE_NAME) | tr A-Z a-z)/files ]; then \
		$(CP) ../$(shell echo $(DEVICE_NAME) | tr A-Z a-z)/files/* $(1)/; \
	fi
endef

# (1) target rootfs directory
define IopPrepareRootfs
	sed -i 's/^IOP Version.*/IOP Version: $(IOPSYS_BUILD_VERSION)/gi' $(1)/etc/banner
	mkdir -p $(1)/etc/board-db/version

	echo $(IOPSYS_BUILD_VERSION)  > $(1)/etc/board-db/version/iop_version
	echo $(CONFIG_TARGET_CUSTOMER)> $(1)/etc/board-db/version/iop_customer

# standard os version info https://www.freedesktop.org/software/systemd/man/os-release.html
	rm $(1)/etc/os-release
	echo "NAME=\"iopsysWrt\""				>$(1)/etc/os-release
	echo "VERSION=\"$(IOPSYS_BUILD_VERSION)\""		>>$(1)/etc/os-release
	echo "ID=\"iopsys\""					>>$(1)/etc/os-release
	echo "ID_LIKE=\"openwrt\""				>>$(1)/etc/os-release
	echo "VARIANT=\"$(CONFIG_TARGET_CUSTOMER)\""		>>$(1)/etc/os-release
	echo "ANSI_COLOR=\"0;34\""				>>$(1)/etc/os-release
# iopsys specific
	echo "IOPSYS_BOARD=\"$(SUBTARGET)\""			>>$(1)/etc/os-release
	echo "GIT_VER=\"$(GIT_VER)\""				>>$(1)/etc/os-release

        # add "first boot" marker (.w.y.y2)
        $(if $(CONFIG_TARGET_NO_1STBOOT_MARK), ,touch $(1)/IOP3)

	$(call InstallExtraFiles,$(KDIR)/$@)
endef

define MakeLzma
	$(STAGING_DIR_HOST)/bin/lzma e $(1) -lc1 -lp2 -pb2 $(1).new
	mv $(1).new $(1)
endef

define MakeUImage
	mkimage -A $(LINUX_KARCH) \
		-O linux -T kernel \
		-C $(1) -a $(KERNEL_LOADADDR) -e $(if $(KERNEL_ENTRY),$(KERNEL_ENTRY),$(KERNEL_LOADADDR)) \
		-n '$(if $(UIMAGE_NAME),$(UIMAGE_NAME),$(call toupper,$(LINUX_KARCH)) LEDE Linux-$(LINUX_VERSION))' -d $(2) $(2).new
	mv $(2).new $(2)
endef

# (1) target rootfs directory
define IopInstallKernel
	mkdir -p $(1)/boot
	cp $(KDIR)/$(DEVICE_NAME)$(KERNEL_SUFFIX) $(1)/boot/uImage
	$(call MakeLzma,$(1)/boot/uImage)
	$(call MakeUImage,lzma,$(1)/boot/uImage)
	$(CP) $(KDIR)/uboot.img $(1)/boot
endef

# (1) target rootfs directory
define IopInstallDTB
	$(foreach dts,$(DEVICE_DTS),cp $(KDIR)/image-$(dts).dtb $(1)/boot/$(dts).dtb; )
endef
