ifeq ($(SUBTARGET),xrx500)

ASTERISK_PACKAGES := asterisk asterisk-app-confbridge asterisk-app-playtones
ASTERISK_PACKAGES += asterisk-app-read asterisk-app-record asterisk-app-system
ASTERISK_PACKAGES += asterisk-bridge-builtin-features asterisk-bridge-simple
ASTERISK_PACKAGES += asterisk-bridge-softmix asterisk-cdr asterisk-cdr-csv
ASTERISK_PACKAGES += asterisk-chan-lantiq asterisk-chan-sip asterisk-format-g726
ASTERISK_PACKAGES += asterisk-format-g729 asterisk-format-gsm asterisk-format-sln
ASTERISK_PACKAGES += asterisk-func-channel asterisk-func-db asterisk-func-shell
ASTERISK_PACKAGES += asterisk-pbx-spool asterisk-res-musiconhold asterisk-res-rtp-asterisk
ASTERISK_PACKAGES += asterisk-res-timing-timerfd asterisk-sounds asterisk-app-transfer
ASTERISK_PACKAGES += asterisk-codec-a-mu asterisk-codec-alaw asterisk-codec-ulaw
ASTERISK_PACKAGES += asterisk-codec-g722 asterisk-codec-g726 asterisk-codec-g729
ASTERISK_PACKAGES += asterisk-codec-gsm asterisk-util-astcanary asterisk-res-srtp

VOICE_PACKAGES := $(ASTERISK_PACKAGES)

COMMON_PACKAGES := \
	kmod-usb-dwc3-grx500 kmod-usb3 kmod-usb-storage kmod-scsi-core kmod-fs-vfat \
	kmod-nls-cp437 kmod-nls-iso8859-1 block-mount \
	wireless-tools bridge ated $(VOICE_PACKAGES)

DEVICE_VARS += UBIFS_OPTS

define Device/EASY550
  $(Device/NAND)
  $(Device/IopsysCommon)
  DEVICE_DTS := easy550_anywan
  DEVICE_TITLE := EASY550 - EASY550 V2 Reference Board
  KERNEL_LOADADDR := 0xa0020000
  KERNEL_ENTRY := 0xa0020000
  DEVICE_PACKAGES := $(COMMON_PACKAGES) uboot-grx500
  UBIFS_OPTS :=-m 2048 -e 126976 -c 4096
  ROOTFS_PREPARE := PrepareRootfsGrxCommon
endef
TARGET_DEVICES += EASY550

define Device/SPEEDPORT_SMART3
  $(Device/NAND)
  $(Device/IopsysCommon)
  DEVICE_DTS := easy550_anywan_Smart3Board
  DEVICE_TITLE := Speedport Smart3
  KERNEL_LOADADDR := 0xa0020000
  KERNEL_ENTRY := 0xa0020000
  DEVICE_PACKAGES := $(COMMON_PACKAGES)
  UBIFS_OPTS :=-m 4096 -e 253952 -c 4096
  ROOTFS_PREPARE := PrepareRootfsGrxCommon
endef
TARGET_DEVICES += SPEEDPORT_SMART3

define Device/EASY350
  $(Device/NAND)
  $(Device/IopsysCommon)
  DEVICE_DTS := easy350
  DEVICE_TITLE := GRX350 11AC Dual Band Wifi VDSL Gateway
  KERNEL_LOADADDR := 0xa0020000
  KERNEL_ENTRY := 0xa0020000
  DEVICE_PACKAGES := $(COMMON_PACKAGES) uboot-grx500
  UBIFS_OPTS :=-m 2048 -e 126976 -c 4096
  ROOTFS_PREPARE := PrepareRootfsGrxCommon
endef
TARGET_DEVICES += EASY350

define Device/NORRLAND
  $(Device/NAND)
  $(Device/IopsysCommon)
  DEVICE_DTS := norrland maia
  DEVICE_TITLE := Norrland Board
  KERNEL_LOADADDR := 0xa0020000
  KERNEL_ENTRY := 0xa0020000
  DEVICE_PACKAGES := $(COMMON_PACKAGES) uboot-grx500_norrland kmod-gryphon-led-kernel-module
  UBIFS_OPTS :=-m 2048 -e 126976 -c 4096
  ROOTFS_PREPARE := PrepareRootfsGrxCommon
endef
TARGET_DEVICES += NORRLAND

# arg (1): target rootfs
define Image/Prepare/PrepareRootfsGrxCommon
	@echo
	@echo Preparing rootfs for $(DEVICE_NAME)

	$(call IopPrepareRootfs,$(1))
	$(call IopInstallKernel,$(1))
	$(call IopInstallDTB,$(1))
endef

endif
