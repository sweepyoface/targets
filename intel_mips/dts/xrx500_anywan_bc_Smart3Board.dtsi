#include <dt-bindings/gpio/gpio.h>
/******************************************************************************
** Board configuration: xRX500 ANYWAN Board
******************************************************************************/
/ {

/******************************************************************************
** Board configuration: Enable buttons on the board.
******************************************************************************/
	ltq_swreset {
	      compatible = "lantiq,ltq_swreset";
	      swreset_pin = <&gpio0 15 1>;
	      swreset_bit = <1>;
	      status = "disabled";
	};
    gpio-keys-polled {
        compatible = "gpio-keys-polled";
        #address-cells = <1>;
        #size-cells = <0>;
        poll-interval = <100>;

        //node.gpios = <'gpio_chip' 'gpio_num' 'low_active'>
        reset {
            label = "reset";
            gpios = <&gpio0 15 0>;
            linux,code = <0x198>;
        };
        dect_wps {
            label = "dect_wps";
            gpios = <&gpio0 17 0>;
            linux,code = <0x211>;
        };
        dect_paging {
            label = "dect_paging";
            gpios = <&gpio0 18 0>;
            linux,code = <0x100>;
        };
        wlan {
            label = "wlan";
            gpios = <&gpio0 16 0>;
            linux,code = <0x101>;
        };
	};
/*****************************************************************************/
};

/******************************************************************************
** Board configuration: Enable fxs SS.
******************************************************************************/
&pinctrl_vcodec {
	status = "disabled";
};

/******************************************************************************
** Board configuration: Enable spi1 SS.
******************************************************************************/

&pinctrl_spi1 {
	status = "ok";
};

/******************************************************************************
** Board configuration: Enable dect SS.
******************************************************************************/
/ {
dect {
	compatible = "lantiq,ltqdect";
	/*lantiq,dect-spi-int = <271>; 271 GPIO base + GPIO 31 for cosic interrupt and driver Probe fn will look for string dect-spi-int */
	/*interrupt-parent = <&gpio0>; */
	/*interrupts = <0 0x2>; */
	lantiq,dect-cs= <0>; /*DECT chip select port Number; This is used by SSC interface*/
	gpio-reset = <&ssogpio 9 0>;
	gpio-int = <&gpio0 0 0>; /*COSIC INT PIN connected to GPIO 0 */
	status = "ok";
	};

dect-page {
	compatible = "lantiq,ltqdect-page";
	lantiq,pagebit= <1>; /*DECT Page Bit: Bit info meant for DECT*/
	//lantiq,pagepin = <&gpio0 7 0>;
	lantiq,pagepin = <&gpio0 18 0>;
	status = "ok";
	};
};

/******************************************************************************
** Board configuration: Enable spi0 configuration.
******************************************************************************/

/******************************************************************************
** PCIe PHY board configuration
******************************************************************************/
&pcie_phy0 {
	status = "ok";
	intel,ssc_enable = <1>;
};

&pcie_phy1 {
	status = "ok";
	intel,ssc_enable = <1>;
};

&pcie_phy2 {
	status = "ok";
	intel,ssc_enable = <1>;
};

/******************************************************************************
** Board configuration: Enable PCIe board configuration.
** PCIE sub-system feature configuration, the pcie0/1/2 are defined in Soc level
******************************************************************************/
&pcie0 {
	/* Intel WAVE 5G WIFI */
	status = "okay";
	intel,rst-interval = <100>;
	intel,inbound-swap = <1>;
	intel,outbound-swap = <0>;
	reset-gpio = <&gpio1 29 0>; // default value , it can be updated on board.
};

&pcie1 {
	/* Intel WAVE 2.4G WIFI */
	status = "okay";
	intel,rst-interval = <100>;
	intel,inbound-swap = <1>;
	intel,outbound-swap = <0>;
	reset-gpio = <&gpio1 0 0>;// default value , it can be updated on board.
};

&pcie2 {
	/* VRX518 - DSL */
	status = "okay";
	intel,rst-interval = <100>;
	intel,inbound-swap = <1>;
	intel,outbound-swap = <0>;
	reset-gpio = <&gpio1 1 0>;// default value , it can be updated on board.
};

/******************************************************************************
** Board configuration: Enable USB board configuration.
******************************************************************************/
/ {
	usb0_vbus:regulator-vbus@0 {
		compatible = "regulator-fixed";
		regulator-name = "usb0_vbus";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&ssogpio 6 0>;
		enable-active-high;
	};

	usb1_vbus:regulator-vbus@1 {
		compatible = "regulator-fixed";
		regulator-name = "usb1_vbus";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio0 2 0>;
		enable-active-high;
	};
};

&usb0_phy {
	vbus-supply = <&usb0_vbus>;
};

&usb0 {
	status = "ok";
};

&usb1_phy {
        vbus-supply = <&usb1_vbus>;
};

&usb1 {
	status = "disabled";
};

/******************************************************************************
 ** Board configuration: Enable pinctrl board configuration.
 ******************************************************************************/
&pinctrl {
	pinctrl_spi0_cs1: spi0_cs1 {
		intel,pins =  <15>; /* SPI0_CS1 */
		intel,function = "spi0_cs1";
		intel,mux = <EQBR_MUX_1>;
		intel,groups = "spi0_cs1";
	};

	pinctrl_spi1_cs0: spi1_cs0 {
		intel,pins =  <14>; /* SPI1_CS0 */
		intel,function = "spi1_cs0";
		intel,mux = <EQBR_MUX_2>;
		intel,groups = "spi1_cs0";
	};
};

/******************************************************************************
** Board configuration: Enable Shift register LED board configuration.
******************************************************************************/
&pinctrl_ledc {
			status="ok";
		};

&ssogpio {
	status = "okay";
	intel,sso-update-rate = <250000>;
};

&ssoled {
	status = "okay";
	/* led definition */

	sso,falling;
	sso,default-brightness = <0xFF>;
	sso,default-blinkrate = <4>; /* HZ*/
	/* blink rate list: 2, 4, 8, 10, 50K, 100K, 200K, 250K, 1000K */
	blink,delay = <250>;

	led1: led1 {
		label = "power_green_led";
		led-gpio = <&ssogpio 1 0>;
		led-pin = <1>;
		brightness = <0x00>;
		max-brightness = <40>;
		//sso,hw-trigger;
	};

	led4: led4 {
		label = "service_led";
		led-gpio = <&ssogpio 4 0>;
		led-pin = <4>;
		//sso,hw-trigger;
		active_low;
	};

	led5: led5 {
		label = "dect_paging_led";
		led-gpio = <&ssogpio 5 0>;
		led-pin = <5>;
		//sso,hw-trigger;
		active_low;
	};

	led8: led8 {
		label = "dsl_led";
		led-gpio = <&ssogpio 8 0>;
		led-pin = <8>;
		active_low;
	};


	led10: led10 {
		label = "wlan_led";
		led-gpio = <&ssogpio 10 0>;
		led-pin = <10>;
		active_low;
	};

	led11: led11 {
		label = "online_led";
		led-gpio = <&ssogpio 11 0>;
		led-pin = <11>;
		active_low;
	};

	led12: led12 {
		label = "phone_led";
		led-gpio = <&ssogpio 12 0>;
		led-pin = <12>;
		active_low;
	};

	led13: led13 {
		label = "dect_wps_led";
		led-gpio = <&ssogpio 13 0>;
		led-pin = <13>;
		active_low;
	};

	led14: led14 {
		label = "power_red_led";
		led-gpio = <&ssogpio 14 0>;
		led-pin = <14>;
		brightness = <0x00>;
	};

	led15: led15 {
		label = "power_white_led";
		led-gpio = <&ssogpio 15 0>;
		led-pin = <15>;
		//linux,default-trigger = "default-on";
		active_low;
		linux,default-trigger = "timer";
	};
};

//&sso {
//	status = "okay";
//	lantiq,phy2 = <0x0>;
//	lantiq,phy3 = <0x0>;
//	lantiq,phy4 = <0x0>;
//	lantiq,phy5 = <0x0>;
//	lantiq,phy6 = <0x0>;
//	lantiq,groups = <0x3>;
//	/* lantiq,rising; */
//	blink,delay = <250>;
//	#address-cells = <1>;
//	#size-cells = <0>;
//
//	power_green_led {
//		label = "power_green_led";
//		reg = <0x1>;
//	};
//
//	power_white_led {
//		label = "power_white_led";
//		reg = <0xf>;
//		//linux,default-trigger = "timer";
//		active_low;
//	};
//
//	power_red_led {
//		label = "power_red_led";
//		reg = <0xe>;
//	};
//
//	dsl_led {
//		label = "dsl_led";
//		reg = <0x8>;
//		active_low;
//	};
//
//	online_led {
//		label = "online_led";
//		reg = <0xb>;
//		active_low;
//	};
//
//	phone_led {
//		label = "phone_led";
//		reg = <0xc>;
//		active_low;
//	};
//
//	service_led {
//		label = "service_led";
//		reg = <0x4>;
//		active_low;
//	};
//
//	wlan_led {
//		label = "wlan_led";
//		reg = <0xa>;
//		active_low;
//	};
//
//	dect_wps_led {
//		label = "dect_wps_led";
//		reg = <0xd>;
//		active_low;
//	};
//
//	dect_paging_led {
//		label = "dect_paging_led";
//		reg = <0x5>;
//		active_low;
//	};
//};

/******************************************************************************
** Board configuration: Enable TDM setting.   
******************************************************************************/
&pinctrl_tdm {
	status = "ok";
};

/******************************************************************************
** Board configuration: Enable CoC power managment board feature
******************************************************************************/
&pinctrl_i2c0 {
			status="ok";
};

&cpu0 {
	cpu-supply = <&buck2_reg>;
};

&i2c {
	status = "ok";
	//ina219: ina219@40 {
	//	compatible = "ti,ina219";
	//	reg = <0x40>;
	//	shunt-resistor = <10000>;
	//	rail-name = "PWR_12V";
	//	};

	dcdc:tps65273@62 {
		status = "ok";
		compatible = "ti,tps65273";
		reg = <0x62>;

		regulators {
			dcdc2 {
				regulator-name = "1V15VDD-dcdc2";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1150000>;
				regulator-max-microvolt = <1200000>;
				regulator-microvolt-offset = <0>;

				vout-slew-rate = <0>;
				vout-psm-mode  = <0>;
			};
			buck1_reg: BUCK1 {
				regulator-name = "1V5VDD";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1500000>;
				regulator-max-microvolt = <1500000>;
				regulator-microvolt-offset = <0>;
				vout-slew-rate = <0>;
				vout-psm-mode  = <0>;
				vout-init-selector = <82>; /*0x52 - 1500000 uV*/
			};

			buck2_reg: BUCK2 {
				regulator-name = "1V15VDD";
				regulator-always-on;
				regulator-boot-on;
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1200000>;
				regulator-microvolt-offset = <0>;

				vout-slew-rate = <0>;
				vout-psm-mode  = <0>;
				vout-init-selector = <47>; /*0x2F - 1150000 uV*/
			};
		};
	};

	stm24l: stm24l@40 {
		compatible = "arcadyan,stm24l";
		reg = <0x40>;
	};
};

/******************************************************************************
** Board configuration: Enable SSC1 to support standard SPI devices (SPI Flash)
******************************************************************************/
&ssc1 {
		status="ok";
		mt29f@0 {
				#address-cells = <1>;
				#size-cells = <1>;
				reg = <0 1>;
				compatible = "spinand,mt29f";
				linux,modalias = "mt29f";
				spi-max-frequency = <1000000>;

				partition@0 {
					label = "uboot";
					reg = <0x000000 0x100000>;
				};

				partition@100000 {
					label = "data";
					reg = <0x100000 0x1000000>;
				};

				partition@1100000 {
					label = "res";
					reg = <0x1100000 0x6E00000>;
				};
			};
};

/******************************************************************************
** Board configuration: Configure LAN/WAN interfaces
******************************************************************************/
&eth {
	status = "ok";

	lan0: interface@0 {
		compatible = "lantiq,xrx500-pdi";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <1>;
		intel,dp-dev-port = <1>;
		intel,dp-port-id = <1>;

		ethernet@1 {
			compatible = "lantiq,xrx500-pdi-port";
			reg = <1>;
			phy-mode = "rgmii";
		};
	};

	lan1: interface@1 {
		compatible = "lantiq,xrx500-pdi";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <1>;
		intel,dp-dev-port = <2>;
		intel,dp-port-id = <2>;

		ethernet@1 {
			compatible = "lantiq,xrx500-pdi-port";
			reg = <2>;
			interrupt-parent = <&gic>;
			interrupts = <0 112 4>;
			phy-mode = "rgmii";
			phy-handle = <&phy2>;
		};
	};

	lan2: interface@2 {
		compatible = "lantiq,xrx500-pdi";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <2>;
		intel,dp-dev-port = <3>;
		intel,dp-port-id = <3>;

		ethernet@2 {
			compatible = "lantiq,xrx500-pdi-port";
			reg = <3>;
			interrupt-parent = <&gic>;
			interrupts = <0 113 4>;
			phy-mode = "rgmii";
			phy-handle = <&phy3>;
		};
	};

	lan3: interface@3 {
		compatible = "lantiq,xrx500-pdi";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <3>;
		intel,dp-dev-port = <4>;
		intel,dp-port-id = <4>;

		ethernet@3 {
			compatible = "lantiq,xrx500-pdi-port";
			reg = <4>;
			interrupt-parent = <&gic>;
			interrupts = <0 126 4>;
			phy-mode = "rgmii";
			phy-handle = <&phy4>;
		};
	};

	lan4: interface@4 {
		compatible = "lantiq,xrx500-pdi";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <4>;
		intel,dp-dev-port = <5>;
		intel,dp-port-id = <5>;

		ethernet@4 {
			compatible = "lantiq,xrx500-pdi-port";
			reg = <5>;
			interrupt-parent = <&gic>;
			interrupts = <0 127 4>;
			phy-mode = "rgmii";
			phy-handle = <&phy5>;
		};
	};

	wan: interface@5 {
		compatible = "lantiq,xrx500-pdi";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <5>;
		lantiq,wan = <1>;
		intel,dp-dev-port = <15>;
		intel,dp-port-id = <15>;

		ethernet@5 {
			compatible = "lantiq,xrx500-pdi-port";
			reg = <1>;
			interrupt-parent = <&gic>;
			interrupts = <0 111 4>;
			phy-mode = "rgmii";
			phy-handle = <&phy1>;
		};
	};
};

