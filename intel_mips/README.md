Summary
=======

This document gives a brief introduction about the intel_mips architecture as
well as it describes the process of adding a new intel_mips board for IopsysWrt.

IopsysWrt Flash Layout
======================

In intel_mips, IopsysWrt divides the flash into two MTD partitions. They are described in the following table:
```
#: name                size             offset
0: uboot               0x00100000       0x00000000
1: ubi                 <remaining>      0x00100000
```
- uboot partition is 1MB size and it contains the u-boot binary
- ubi partition is the remaining space and it contains the UBI volumes

Note that the last valid four Erase Blocks of the flash will be automatically reserved for
on-flash bad block table.

The UBI volumes is partitioned as the following:
```
#: name			size
0: env1			One logical Erase Block
1: env2			One logical Erase Block
2: rootfs_0		<depend on your flash size>
3: rootfs_1		<depend on your flash size>
4: usr_data		<depend on your choice>
```
- env1/env2: they are used to store u-boot environment variables
- rootfs_0/rootfs_1: each one of them contain a running system, meaning a kernel and a root file system
- usr_data: This volume is optional and it's not needed or used by IopsysWrt. However if you create it,
then IopsysWrt will automatically mount it for you.

Bootloader
==========

The bootloader for intel_mips is called u-boot. IopsysWrt add some functionality to it.
The IopsysWrt/openwrt package makefile of u-boot is located in feeds/iopsys/uboot/Makefile.
It provides the steps needed to download and compile the package.

When building an IopsysWrt image, u-boot will be downloaded in the following path:
```
build_dir/target-mips_24kc+nomips16_musl/u-boot-grx500_<BOARD>
```

To make the porting process of u-boot easier, you may download it separately in another location as the following:
```
$ git clone git@dev.iopsys.eu:intel/uboot.git
```

The current commit id of u-boot for intel_mips is defined by the variable PKG_SOURCE_VERSION as the following:
```
ifdef CONFIG_TARGET_intel_mips
PKG_SOURCE_URL:=git@dev.iopsys.eu:intel/uboot.git
else
PKG_SOURCE_VERSION:=c77089efc41ea3292125330221bd5016796631b9
```

You may check the current branch with the following:
```
$ git branch -r --contains <PKG_SOURCE_VERSION>
```

example:
```
$ git branch -r --contains c77089efc41ea3292125330221bd5016796631b9
  origin/iop_grx-ugw-8.1.1-ga
$
```

Check out the the branch with the following:
```
$ git checkout origin/iop_grx-ugw-8.1.1-ga -b iop_grx-ugw-8.1.1-ga
```

The first step in porting u-boot is defining/writing the configuration file of u-boot for the new board.
You may start this by copying existing one, like configs/grx500_norrland_defconfig.
Adapt the configuration to your board, then some customization might be needed in u-boot source code depend
on your board and your need.

Building U-Boot
---------------

First, you will need the toolchain for intel_mips, the easiest way to get it is simply build
an existing intel_mips board.

In order to build u-boot in standalone way, you need to prepare the environment as the following:
```
ROOTDIR=path/to/iopsyswrt
export PATH=$ROOTDIR/staging_dir/host/bin:$ROOTDIR/staging_dir/toolchain-mips_24kc+nomips16_gcc-7.4.0_musl/bin:$PATH
export CROSS_COMPILE=mips-openwrt-linux-musl-
export ARCH=mips
```
Now you can try to build existing board
```
$ make grx500_norrland_defconfig
$ make
```

If the build is successful, then it will will generate u-boot-nand.bin file and this is the file to write
on the first MTD partition "uboot" on the offset 0x0

Flashing u-boot
---------------

If you have the default intel_mips uboot, then you can write/migrate to IopsysWrt u-boot in the following way:
```
# setenv ipaddr 192.168.1.1
# setenv serverip 192.168.1.2
# tftp u-boot-nand.bin
# nand erase 0 0x100000
# nand write.partial ${loadaddr} 0 ${filesize}
```
Be aware the steps above could brick you board if something goes wrong.
Still most intel_mips boards will be able to recover by booting from UART.

Once u-boot is flashed, you need to partition the nand flash.
This can be done with the help of the macro format_system_1 and format_system_2.
For NORRLAND board the content of these macros is the following:
```
format_system_1=nand erase.part ubi; reset
format_system_2=ubi create env1 1f000;ubi create env2 1f000;ubi create rootfs_0 3a20000;ubi create rootfs_1 3a20000;ubi create usr_data 22e000;saveenv
```
Adapt the sizes of the ubi volumes according to flash size and Erase Block size.

To format the nand, proceed as the following:
```
# run format_system_1
The board will reboot
# run format_system_2
```
The above steps to format the nand flash are suitable for the development environment.
For the production a raw nand image that can be flashed by a nand programmer is more suitable.

Adding The New U-Boot To The Package Makefile
---------------------------------------------

Assuming we have a board with a configuration file called grx500_newboard_defconfig
then we have to define a new package called grx500_newboard
```
define uboot/grx500_newboard
  TITLE:=U-Boot for newboard
  UBOOT_IMG:=u-boot-nand.bin
  DEPENDS:=@TARGET_intel_mips
  PKG_RELEASE:=1
endef
```
and then add it the to variable UBOOTS.
```
UBOOTS := \
	ex400 \
	grx500 \
	grx500_speedport_smart3 \
	grx500_norrland \
	grx500_newboard
```
You may increment the variable PKG_RELEASE whenever you want IopsysWrt to update uboot on the flash.

When you build IopsysWrt image for intel_mips board, the uboot binary image will be called uboot.img,
and it can be found under the directory bin/targets/intel_mips/xrx500/

Booting
-------

The u-boot environment variable "bootcmd" define the commands to be executed to boot the system.
Ideally IopsysWrt needs bootcmd to be set to "rescue;iboot".

The command "rescue" handles the boot rescue feature for IopsysWrt.
The command "iboot" will load the device tree blob and the kernel, and consequently start the kernel and the root file system.

the iboot command needs the file name of the device tree blob to be loaded. It can be set in two ways:
- By setting the u-boot environment variable "fdtfile" to device tree blob file name.
- By guessing the value of fdtfile based on some customized criteria. See Norrland example in u-boot source code.

Booting From UART
-----------------

Booting from UART is useful if you flash a bad u-boot image. You will need however to generate a special binary for that.
To do so, you may run the following commands:
```
$ chmod +x ./board/lantiq/grx500/build_asc_gphy.pl
$ ./board/lantiq/grx500/build_asc_gphy.pl board/lantiq/grx500/uartddr.conf u-boot-gphy.bin 0xa0400000 ./board/lantiq/grx500/gphy_firmware.img 0xa0220000 u-boot.asc 0
```

Directories and Files Description of intel_mips Target
======================================================

This is summary of some of the important files and directories:
```
/base-files                 base files to be installed on all intel_mips boards
/config                     the different fragments for the  global openwrt configuration
/config-4.9                 kernel configuration of intel_mips, this is a common fragment for all boards
/Config.in                  contains misc configuration about the target. Here you can configure the git commit id for the kernel
/dts                        device tree source code files
/image
	/image-iopsys.mk    required functions to generate iopsys image
	/xrx500-iopsys.mk   the definition of different devices that are supported by iopsys for intel_mips
/<board_id_lower_case>      exclusive files to be installed on a specific board. Files here could overwrite any package files
```

Adding A New Board To IopsysWrt
===============================

Add a new Board definition to the file image/xrx500-iopsys.mk.
You may copy the defintion from an existing board like NORRLAND

Here is an example to add a new board called NEWBOARD to the build system, you may add the following to
image/xrx500-iopsys.mk:
```
define Device/NEWBOARD
  $(Device/NAND)
  $(Device/IopsysCommon)
  DEVICE_DTS := newboard
  DEVICE_TITLE := New Board
  KERNEL_LOADADDR := 0xa0020000
  KERNEL_ENTRY := 0xa0020000
  DEVICE_PACKAGES := $(COMMON_PACKAGES) uboot-grx500_newboard
  UBIFS_OPTS :=-m 2048 -e 126976 -c 4096
  ROOTFS_PREPARE := PrepareRootfsGrxCommon
endef
TARGET_DEVICES += NEWBOARD
```

Variables Description:
- DEVICE_DTS: This is space separated list of device tree files names, but without the file's name extension.
All device tree listed here will be compiled and installed on the root file system.
- DEVICE_TITLE: Ornamental description for the device.
- KERNEL_LOADADDR: Usually it's 0xa0020000
- KERNEL_ENTRY:  Usually, it's 0xa0020000
- DEVICE_PACKAGES: List of packages to be installed on the board
- UBIFS_OPTS: This variable defines parameters to be passed to mkfs.ubifs program. They need to match to flash characteristics
- ROOTFS_PREPARE: A custom hook to be executed on the root file system. You may use the default one.

The device tree needs to describe your hardware. However IopsysWrt require the following criteria:
- The first token of top node's property "model" need to define a board id. Example "NORRLAND (GRX350) Main model". Here NORRLAND is the board id.
- The flash driver need to enable the nand-on-flash-bbt property. This can be done by adding the line `nand-on-flash-bbt;` to the node `nand-parts@0`.
- The MTD partitions defined in `nand-parts@0` can be removed, as they are not used by IopsysWrt

UCI Board DataBase
------------------

You will need to add a new UCI DB file for the new board. The DB is located in base-files/etc/board-db/boards/
and the file name needs to match the board id that is defined by the device tree's property model.
As example for Norrland this is base-files/etc/board-db/boards/NORRLAND

The first part of the UCI DB must contains the following sections:
```
config board 'board'
	option hardware '<BOARD ID>'
	option hasAdsl '0'
	option hasVdsl '0'
	option hasVoice '1'
	option hasDect '0'
	option hasUsb '1'
	option hasCatv '0'
	option hasSfp '0'
	option VoicePorts '2'
	option VoicePortNames 'Tel_1 Tel_2'
	option VoicePortOrder 'tapi0 tapi1'
	option hasWifi '1'
	option ethernetPortNames 'LAN1 LAN2 LAN3 LAN4 WAN'
	option ethernetPortOrder 'eth0_1 eth0_2 eth0_3 eth0_4 eth1'
	option ethernetLanPorts 'eth0_1 eth0_2 eth0_3 eth0_4'
	option ethernetWanPort 'eth1'
```
Replace `<BOARD ID>` with the actual board id and adapt the other parameters to your board.

LED
---

intel_mips implements standard linux LED, However a board could also use other type of the LED.
In this example we will describe how to configure Linux LED.

Firstly you need to configure the LEDs in the device tree to match the hardware characteristics.
Then you need to configure those LEDs in the UCI DB, in order to get IopsysWrt support them.

Here is example that show the configuration for some LED:

In the following code fragment, we add a LED called wireless_green to the list of LEDs
```
config gpio_led gpio_leds
    list leds wireless_green
```
The following section will configure wireless_green as linux led thanks to the mode option.
The sysfs_attr parameter describe the led name in /sys/class/leds.
The "active" option can be set hi or low depend on your led polarity
```
config gpio_led 	wireless_green
    option sysfs_attr	led6:green:wireless
    option active	low
    option mode		led_linux
```
In this section we create a ubus object called led_wifi and we configure each action for different state:
```
config led_map led_wifi
    list led_action_ok     'wireless_green = ON'
    list led_action_off    'wireless_green = OFF'
    list led_action_notice 'wireless_green = FLASH_SLOW'
    list led_action_alert  'wireless_green = FLASH_FAST'
    list led_action_error  'wireless_green = FLASH_FAST'
```
Note that for Norrland, IopsysWrt implements the following ubus LED: catv, wifi, voice, internet, broadband.

For more elaborate example, see base-files/etc/board-db/boards/NORRLAND

Note that for Norrland, they are two scripts that controls the LED behavior, those scripts are:
- base-files/etc/init.d/switch_leds: It configures the ethernet port's LED which are controlled by hardware.
- norrland/files/etc/hotplug.d/switch/10-eth-leds: It configures the LED activity behavior of the ethernet ports

GPIO Input (Button)
-------------------

intel_mips implements standard linux gpio input. The configuration of the GPIO input buttons is done in the UCI DB.

In the following example we will describe how to configure a GPIO button.

Here, we add a button called gpio_reset to list of buttons
```
config gpio_button gpio_buttons
    list buttons gpio_reset
```
The following section configures the GPIO number to 480, as well as the polarity as active low.
```
config gpio_button gpio_reset
    option addr 480
    option active low
```
The following will create ubus object RESET as well as hotplug object called resetbutton.
For the reset button it should be named always like that.
The minpress option tell IopsysWrt that the user should press for five seconds before the reset action is triggered
```
config button RESET
	list button gpio_reset
	option minpress 5000
	option hotplug resetbutton
```
Note that you may add further board custom definition in /etc/config/buttons.
Also note that IopsysWrt implements the following buttons: RESET, WPS.

For more elaborate example, see base-files/etc/board-db/boards/NORRLAND

Optical Fiber
-------------

If you have an optical fiber, then you will need to enable the external RGMII.
To do that, you may set the variable ENABLE_EXTERNAL_RGMII to 1 in the script base-files/etc/init.d/external-rgmii
