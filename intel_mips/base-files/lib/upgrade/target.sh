


#--------------------------------------------------------------
get_chip_id() {
#	awk '/^system type/ && $5 == "MT7621" { printf "7621" }' /proc/cpuinfo
    echo "grx550"
}



#--------------------------------------------------------------
# Upgrade for ubi flash layout. When we get here
# sanity checks of the image has already been done.
target_upgrade() {
	local cur_vol upd_vol upd_vol_name upd_vol_id
	local ubifs_ofs ubifs_sz
	local from="$1"

	ubifs_ofs=$(get_section_offset $from ubifs)
	ubifs_sz=$(get_section_size $from ubifs)
	cur_vol=$(get_flashbank_current)
	upd_vol=$(get_flashbank_next)
	upd_vol_name="rootfs_${upd_vol}"

	[ $ubifs_sz -eq 0 ] && return

	# convert from ubi volume name to ubi id number
	upd_vol_id=$(ubinfo -d 0 -N $upd_vol_name | grep "Volume ID:" |awk '{print $3}')

	v "Writing UBI data to $upd_vol_name volume..."
	grep -q ubi0_$upd_vol_id /proc/mounts && umount -f /dev/ubi0_$upd_vol_id

	# Filesystem + kernel
	ubiupdatevol /dev/ubi0_$upd_vol_id --size=$ubifs_sz \
		--skip=$ubifs_ofs $from || return

	# update u-boot
	mount -t ubifs /dev/ubi0_$upd_vol_id /mnt
	. /lib/upgrade/uboot-upgrade
	if [ -f /mnt/boot/uboot.img ]; then
		v "Trying to update uboot ..."
		uboot_upgrade /mnt/boot/uboot.img
		ret=$?
		if [ $ret -eq 0 ]; then
			v "uboot has been updated successfully"
		elif [ $ret -eq 1 ]; then
			v "uboot update is not needed"
		else
			v "error occured while updating uboot"	
		fi
	fi
	umount /mnt
}



#--------------------------------------------------------------
# Tell the bootloader to boot from flash "bank" $1 next time.
target_set_flashbank_boot() {
	local upd_vol_name

	upd_vol_name="rootfs_${1}"

	# set boot count to 0 as we now have a new system.
	fw_setenv root_vol $upd_vol_name || return
	fw_setenv boot_cnt_primary 0 || return
	fw_setenv boot_cnt_alt 0 || return
}
