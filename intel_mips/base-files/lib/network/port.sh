port_to_id() {
	local port="$1"
	case "$port" in
		eth0_1) echo "2";;
		eth0_2) echo "3";;
		eth0_3) echo "4";;
		eth0_4) echo "5";;
		eth1) echo "1";;
		*);;
	esac
}

get_dev() {
	local port="$1"
	case "$port" in
		eth0_*) echo "0";;
		eth1) echo "1";;
	esac
}

restart_port() {
	local port=$1

	port_id=$(port_to_id $port)
	dev=$(get_dev $port)
	phy_reg=$(switch_cli dev=$dev GSW_MDIO_DATA_READ nAddressDev=$port_id | grep nData | awk -F '\t' '{print $3}' | awk -F ' ' '{print $1}')

	pwr=$((phy_reg & 0x800))

	# if port is up, power down, maintaining rest of settings
	if [ "$pwr" == "0" ]; then
		write_data=$(printf "0x%X\n" $(($phy_reg + 0x800)))
		# keep phy_reg up to date with what is written to register
		phy_reg=$write_data
		switch_cli dev=$dev GSW_MDIO_DATA_WRITE nAddressDev=$port_id nAddressReg=0x0 nData=$write_data > /dev/null 2>&1
	fi

	# power up, maintaining rest of settings
	write_data=$(printf "0x%X\n" $(($phy_reg - 0x800)))
	switch_cli dev=$dev GSW_MDIO_DATA_WRITE nAddressDev=$port_id nAddressReg=0x0 nData=$write_data > /dev/null 2>&1
}

# arg1: port name, ex: eth0
# arg2: port speed, ex: auto/[10-1000][FD/HD]
# arg3: uci section parsed, ex: LAN3
set_port_speed() {
	local port="$1"
	local setting="$2"
	local cfg="$3"
	local speed=$(echo $setting | tr -dc '0-9')
	local duplex
	local auto

	# full auto is equivalent of 1000FDAUTO, set speed manually
	[ "$setting" == "auto" ] && speed="1000"

	local cur_duplex cur_auto cur_speed
	cur_duplex=$(ethtool $port | grep "Duplex:" | awk '{print $2}' | awk '{print tolower($0)}')
	cur_auto=$(ethtool $port | grep "Auto-negotiation:" | awk '{print $2}' | awk '{print tolower($0)}')
	cur_speed=$(ethtool $port | grep "Speed:" | awk '{print $2}' | tr -dc '0-9')

	port_id=$(port_to_id $port)
	dev=$(get_dev $port)
	phy_reg=$(switch_cli dev=$dev GSW_MDIO_DATA_READ nAddressDev=$port_id | grep nData | awk -F '\t' '{print $3}' | awk -F ' ' '{print $1}')

	pwr=$((phy_reg & 0x800))

	case "$setting" in
		disabled)
			# if port is up, power down, maintaining rest of settings
			if [ "$pwr" == "0" ]; then
				write_data=$(printf "0x%X\n" $(($phy_reg + 0x800)))
				switch_cli dev=$dev GSW_MDIO_DATA_WRITE nAddressDev=$port_id nAddressReg=0x0 nData=$write_data > /dev/null 2>&1
				ifconfig $port down
			fi
			return
		;;
		*)
			# if port is down, power up, maintaining rest of settings
			[ "$pwr" != "0" ] && {
				write_data=$(printf "0x%X\n" $(($phy_reg - 0x800)))
				switch_cli dev=$dev GSW_MDIO_DATA_WRITE nAddressDev=$port_id nAddressReg=0x0 nData=$write_data > /dev/null 2>&1
			}

			ifstatus=$(ip link show $port up)
			[ -z "$ifstatus" ] && ifconfig $port up

			case "$setting" in
				*HD*) duplex="half";;
				*) duplex="full";;
			esac
			case "$setting" in
				auto) auto="on";;
				*AUTO) auto="on";;
				*) auto="off";;
			esac
		;;
	esac

	# if autoneg, duplex and cur speed are the same AND config did, no additional change required
	[ "$cur_auto" = "$auto" -a "$cur_duplex" = "$duplex" -a "$cur_speed" = "$speed" ] && return

	advertise() {
		local speed=$1
		local duplex=$2
		local auto=$3
		local port=$4
		local advertise

		[ "$auto" = "off" ] && return

		case "$speed" in
			1000)
				advertise="15"
				[ "$duplex" = "full" ] && advertise="3f"
			;;
			100)
				advertise="05"
				[ "$duplex" = "full" ] && advertise="0f"
			;;
			10)
				advertise="01"
				[ "$duplex" = "full" ] && advertise="03"
			;;
			*)
				return
			;;
		esac

		advertise="0x0$advertise"
		ethtool -s $port advertise $advertise
	}

	ethtool -s $port speed $speed duplex $duplex autoneg $auto
	advertise $speed $duplex $auto $port
}

# arg1: port ifname, ex: eth0
# arg2: port enabled, ex: 1
power_updown() {
	local ifname="$1"
	local enabled=$2

	local port_id=$(port_to_id $ifname)
	local dev=$(get_dev $ifname)
	local phy_reg=$(switch_cli dev=$dev GSW_MDIO_DATA_READ nAddressDev=$port_id | grep nData | awk -F '\t' '{print $3}' | awk -F ' ' '{print $1}')
	local pwr=$((phy_reg & 0x800))
	local write_data

	# if port is disabled but up, power down while maintaining port settings
	if [ $enabled -eq 0 -a "$pwr" == "0" ]; then
		write_data=$(printf "0x%X\n" $(($phy_reg + 0x800)))
		switch_cli dev=$dev GSW_MDIO_DATA_WRITE nAddressDev=$port_id nAddressReg=0x0 nData=$write_data > /dev/null 2>&1
	fi

	# if port is enabled but down, power up while maintaining port settings
	if [ $enabled -eq 1 -a "$pwr" != "0" ]; then
		write_data=$(printf "0x%X\n" $(($phy_reg - 0x800)))
		switch_cli dev=$dev GSW_MDIO_DATA_WRITE nAddressDev=$port_id nAddressReg=0x0 nData=$write_data > /dev/null 2>&1
	fi
}

# arg1: port ifname, ex: eth0
# arg2: port enabled, ex: 1
# arg3: port speed, ex: 1000
# arg4: port duplex, ex: full
# arg5: port autoneg, ex: on
# arg6: port eee, ex: 0
# arg7: port pause, ex: 0
set_port_settings() {
	local ifname="$1"
	local enabled=$2
	local speed="$3"
	local duplex=$4
	local autoneg=$5
	local eee=$6
	local pause=$7

	[ "$ifname" == "eth1" ] && return

	[ -d /sys/class/net/$ifname ] || return

	[ $autoneg -eq 1 ] && autoneg="on" || autoneg="off"
	ethtool --change $ifname speed $speed duplex $duplex autoneg $autoneg 2>/dev/null

	if [ "$autoneg" == "on" ]; then
		local a10MbtH=0x001		#10baseT Half
		local a10MbtF=0x002		#10baseT Full
		local a100MbtH=0x004		#100baseT Half
		local a100MbtF=0x008		#100baseT Full
		local a1GbtH=0x010		#1000baseT Half
		local a1GbtF=0x020		#1000baseT Full
		local a2p5GbtF=0x800000000000	#2500baseT Full
		local a5GbtF=0x1000000000000	#5000baseT Full
		local a10GbtF=0x1000		#10000baseT Full

		local advertise=0x0

		if [ $speed -ge 10 ]; then
			advertise=$((advertise+a10MbtH))
			advertise=$((advertise+a10MbtF))
		fi
		if [ $speed -ge 100 ]; then
			advertise=$((advertise+a100MbtH))
			advertise=$((advertise+a100MbtF))
		fi
		if [ $speed -ge 1000 ]; then
			advertise=$((advertise+a1GbtH))
			advertise=$((advertise+a1GbtF))
		fi
		if [ $speed -ge 2500 ]; then
			advertise=$((advertise+a2p5GbtF))
		fi
		if [ $speed -ge 5000 ]; then
			advertise=$((advertise+a5GbtF))
		fi
		if [ $speed -ge 10000 ]; then

			advertise=$((advertise+a10GbtF))
		fi

		if [ "$advertise" != "0x0" ]; then
			advertise=$(printf "0x%X" $advertise)
			ethtool --change $ifname advertise $advertise
		fi
	fi

	[ $eee -eq 1 ] && eee="on" || eee="off"
	ethtool --set-eee $ifname eee $eee 2>/dev/null

	#case $pause in
	#	off|0)
	#		auto=off
	#		rx=off
	#		tx=off
	#	;;
	#	on|1)
	#		auto=off
	#		rx=on
	#		tx=on
	#	;;
	#	auto)
	#		auto=on
	#		rx=on
	#		tx=on
	#	;;
	#	tx)
	#		auto=off
	#		rx=off
	#		tx=on
	#	;;
	#	rx)
	#		auto=off
	#		rx=on
	#		tx=off
	#	;;
	#esac
	#ethtool --pause $ifname autoneg $auto rx $rx tx $tx 2>/dev/null

	power_updown $ifname $enabled
}
