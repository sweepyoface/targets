#!/bin/sh

do_intel_mips() {
	. /lib/intel_mips.sh

	intel_mips_board_detect
}

boot_hook_add preinit_main do_intel_mips
