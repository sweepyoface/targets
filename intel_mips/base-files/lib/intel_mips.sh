#!/bin/sh
#
# Copyright (C) 2010-2013 OpenWrt.org
#

INTEL_MIPS_BOARD_NAME=
INTEL_MIPS_MODEL=

intel_mips_board_detect() {
	local machine
	local name

	machine=$(awk 'BEGIN{FS="[ \t]+:[ \t]"} /machine/ {print $2}' /proc/cpuinfo | awk '{print $1}')

	case "$machine" in
	*"EASY550")
		name="easy550"
		;;
	*)
		name="generic"
		;;
	esac

	[ -z "$INTEL_MIPS_BOARD_NAME" ] && INTEL_MIPS_BOARD_NAME="$name"
	[ -z "$INTEL_MIPS_MODEL" ] && INTEL_MIPS_MODEL="$machine"

	[ -e "/tmp/sysinfo/" ] || mkdir -p "/tmp/sysinfo/"

	echo "$INTEL_MIPS_BOARD_NAME" > /tmp/sysinfo/board_name
	echo "$INTEL_MIPS_MODEL" > /tmp/sysinfo/model
}

intel_mips_board_name() {
	local name

	[ -f /tmp/sysinfo/board_name ] && name=$(cat /tmp/sysinfo/board_name)
	[ -z "$name" ] && name="unknown"

	echo "$name"
}
