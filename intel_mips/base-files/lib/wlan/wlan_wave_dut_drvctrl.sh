#!/bin/sh

script_name="$0"

[ ! "$LIB_COMMON_SOURCED" ] && . /lib/wlan/wlan_wave_lib_common.sh

drvhlpr_dut="drvhlpr_dut"

command=$1

#TODO: do we really need this?
#
# Find the interface index of wlan0
interface_index=`find_index_from_interface_name wlan0`

drvhlpr_app="/bin/drvhlpr"
dut_file_saver_file=/lib/wlan/wlan_wave_dut_file_saver.sh

#
#TODO: check if this is needed
#
#if [ -e /tmp/dut_aps_and_scripts.sh ]
#then
#	print2log $interface_index DEBUG "/tmp/dut_aps_and_scripts.sh exist, use it"
#	. /tmp/dut_aps_and_scripts.sh > /dev/null
#	print2log $interface_index DEBUG "dut_file_saver_file=$dut_file_saver_file"
#	print2log $interface_index DEBUG "drvhlpr_app=$drvhlpr_app"
#fi

################################################################

start_dut_drvctrl()
{
	# Start drvhlpr for dut
	logger -t drvhlpr-ctrl -p 6 "Starting DUT helper"
	cp -s $drvhlpr_app /tmp/$drvhlpr_dut
	echo "f_saver = $dut_file_saver_file" > /tmp/${drvhlpr_dut}.config
	/tmp/$drvhlpr_dut --dut -p /tmp/${drvhlpr_dut}.config &
}

stop_drvhlpr()
{
	logger -t drvhlpr-ctrl -p 6 "Stopping DUT helper"
	# kill dutserver
	killall $drvhlpr_dut 2>/dev/null

	# So far iopsys don't create driver helper with drvhlpr_wlanX
	#killall drvhlpr_wlan0 2>/dev/null
	#killall drvhlpr_wlan2 2>/dev/null
	#drvhlpr_count=`ps | grep drvhlpr_wlan'[0-1]\{1\}' -c`
	#while [ $drvhlpr_count -gt 0 ]; do echo wait_drvhlpr_close > /dev/console; sleep 1; drvhlpr_count=`ps | grep drvhlpr_wlan'[0-1]\{1\}' -c`; done

	logger -t drvhlpr-ctrl -p 6 "Stop DUT Helper Done"
}

case $command in
	start)
		start_dut_drvctrl
	;;
	stop)
		stop_drvhlpr
	;;
	*)
		echo "$script_name: Unknown command=$command" > /dev/console
	;;
esac
