#!/bin/sh
#
# Callback script for dsl_cpe_control
#
# Copyright (C) 2019, Iopsys Software Solutions AB
#
# Author: Yalu Zhang <yalu.zhang@iopsys.eu>
#

[ "$DSL_NOTIFICATION_TYPE" = "DSL_INTERFACE_STATUS" -a -n "$DSL_INTERFACE_STATUS" ] && {
	# Convert the link status to the canonical string
	case $DSL_INTERFACE_STATUS in
		READY)
			link_status="idle"
			;;
		HANDSHAKE)
			link_status="handshake"
			;;
		TRAINING)
			link_status="training"
			;;
		UP)
			link_status="up"
			;;
		DOWN)
			link_status="down"
			;;
		*)
			echo "Unknown link status $DSL_INTERFACE_STATUS, ignored"
			exit 1
			;;
	esac

	if [ -n "$DSL_XTU_STATUS" ]; then
		dsl_mode=$DSL_XTU_STATUS
	else
		dsl_mode="UNKNOWN"
	fi

	# Send a broadcast UBUS message for link status change notification
	ubus send "dsl" "{\"line\":0,\"status\":\"$link_status\",\"mode\":\"$dsl_mode\"}"

	# Call the hotplug script in background to prevent the calling process from being blocked
	# due to a potentially long execution time
	STATUS=$link_status MODE=$dsl_mode /sbin/hotplug-call dsl &
}

