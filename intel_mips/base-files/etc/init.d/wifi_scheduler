#!/bin/sh /etc/rc.common

START=25
STOP=85

USE_PROCD=1

. /lib/functions.sh

SCHTMATCH=0

EXTRA_COMMANDS="apply_rules"

wifi_updown() {
	local action="$1"
	local disabled=""

	if [ "$action" == "up" ]; then
		logger -t "wifi_scheduler" "Turn WiFi ON"
		disabled="0"
	elif [ "$action" == "down" ]; then
		logger -t "wifi_scheduler" "Turn WiFi OFF"
		disabled="1"
	else
		logger -t "wifi_scheduler" "Disable Scheduler"
		disabled=""
	fi

	set_disabled() {
		local cfg=$1
		uci -q set wireless.$cfg.disabled="$disabled"
		uci -q set wireless.$cfg.disabled="$disabled"
	}

	config_foreach set_disabled wifi-device
	uci commit wireless

	wifi $wifiact
}

day_to_number() {
	case $1 in
		all)		echo 0-6 ;;
		weekdays)	echo 1-5 ;;
		weekend)	echo 0,6 ;;
		sun*)		echo 0 ;;
		mon*)		echo 1 ;;
		tue*)		echo 2 ;;
		wed*)		echo 3 ;;
		thu*)		echo 4 ;;
		fri*)		echo 5 ;;
		sat*)		echo 6 ;;
		*)		echo error ;;
	esac
}

set_wifi_schedule() {
	local cfg="$1"
	local status="$2"
	local days time start stop start_hour stop_hour start_min stop_min
	local sta revsta day dayn dayns current_day current_time daymatch

	config_get days $cfg days
	config_get time $cfg time

	if [ "$status" == "1" ]; then
		sta="up"
		revsta="down"
	else
		sta="down"
		revsta="up"
	fi

	current_day="$(date | awk '{print$1}' | tr 'A-Z' 'a-z')"
	current_time="$(date | awk '{print$4}' | awk -F':' '{print$1$2}')"

	start=$(echo $time | awk -F '[ ,-]' '{print$1}')
	stop=$(echo $time | awk -F '[ ,-]' '{print$2}')

	start_hour=$(echo $start | awk -F ':' '{print$1}')
	start_min=$(echo $start | awk -F ':' '{print$2}')

	stop_hour=$(echo $stop | awk -F ':' '{print$1}')
	stop_min=$(echo $stop | awk -F ':' '{print$2}')

	[ "${start_min//[0-9]/}" = "" ] || return
	[ "${start_hour//[0-9]/}" = "" ] || return
	[ "${stop_min//[0-9]/}" = "" ] || return
	[ "${stop_hour//[0-9]/}" = "" ] || return

	daymatch=0
	for day in $days; do
		[ "${day:0:3}" == "$current_day" ] && daymatch=1
		dayn=$(day_to_number $day)
		[ -n "$dayns" ] && dayns="$dayns,$dayn" || dayns="$dayn"
	done

	[ "${dayns//[0-6,\-]/}" = "" ] || return

	if [ $daymatch -eq 1 -a $current_time -ge ${start/:/} -a $current_time -lt ${stop/:/} ]; then
		SCHTMATCH=1
	fi

	echo "$start_min $start_hour * * $dayns logger -t \"wifi_scheduler\" \"Bring WiFi $sta\"; /etc/init.d/wifi_scheduler apply_rules # WiFi_Schedule" >> /etc/crontabs/root
	echo "$stop_min $stop_hour * * $dayns logger -t \"wifi_scheduler\" \"Bring WiFi $revsta\"; /etc/init.d/wifi_scheduler apply_rules # WiFi_Schedule" >> /etc/crontabs/root

	_NEWCRONMD5=$(md5sum /etc/crontabs/root | awk '{print$1}')

	[ "$_OLDCRONMD5" == "$_NEWCRONMD5" ] || /etc/init.d/cron restart
}

wifi_schedule() {
	local schedule sched_status revstatus
	local was_scheduled=0

	_OLDCRONMD5=$(md5sum /etc/crontabs/root | awk '{print$1}')

	grep -q WiFi_Schedule /etc/crontabs/root && {
		was_scheduled=1
		sed -i "/WiFi_Schedule/ d" /etc/crontabs/root
	}

	config_load wireless

	config_get_bool schedule status schedule "0"

	if [ "$schedule" == "0" ]; then
		[ "$was_scheduled" == "1" ] && wifi_updown
		return
	else
		echo "# WiFi_Schedule" >> /etc/crontabs/root
	fi

	config_get_bool sched_status status sched_status "0"

	config_foreach set_wifi_schedule wifi-schedule $sched_status

	if [ $SCHTMATCH -eq 1 ]; then
		[ $sched_status == "1" ] && wifiact="up" || wifiact="down"
	else
		[ $sched_status == "0" ] && wifiact="up" || wifiact="down"
	fi
	wifi_updown $wifiact
}


apply_rule() {
	local cfg="$1"
	local sched_status="$2"
	local days time start stop

	config_get days $cfg days
	config_get time $cfg time

	current_day="$(date | awk '{print$1}' | tr 'A-Z' 'a-z')"
	current_time="$(date | awk '{print$4}' | awk -F':' '{print$1$2}')"

	start=$(echo $time | awk -F '[ ,-]' '{print$1}')
	stop=$(echo $time | awk -F '[ ,-]' '{print$2}')

	daymatch=0
	for day in $days; do
		[ "${day:0:3}" == "$current_day" ] && daymatch=1
	done

	if [ $daymatch -eq 1 -a $current_time -ge ${start/:/} -a $current_time -lt ${stop/:/} ]; then
		SCHTMATCH=1
	fi
}

apply_rules() {
	config_load wireless

	config_get_bool schedule status schedule "0"
	[ "$status" = "0" ] && return

	config_get_bool sched_status status sched_status "0"
	config_foreach apply_rule wifi-schedule $sched_status

	if [ $SCHTMATCH -eq 1 ]; then
		[ $sched_status == "1" ] && wifiact="up" || wifiact="down"
	else
		[ $sched_status == "0" ] && wifiact="up" || wifiact="down"
	fi

	wifi_updown $wifiact
}

start_service() {
	wifi_schedule
}

service_triggers()
{
	procd_add_reload_trigger wireless
}

